from __future__ import annotations
from dataclasses import dataclass
from enum import Enum

from lidar.datamodel import CartessianCoords

@dataclass
class TrackedObject:
	position: CartessianCoords
	velocity: CartessianCoords
	size: float
