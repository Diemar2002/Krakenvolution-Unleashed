from __future__ import annotations
from functools import partial
from multiprocessing.synchronize import Event
from queue import Empty
from typing import Any, Dict, Optional
from tracker.datamodel import TrackedObject
from lidar.clustering import Object
import threading as th

import egoros

import norfair as nf
import multiprocessing as mp
import numpy as np

update_tracker_ev = mp.Event()

should_stop: bool = False

def init(handler: egoros.TopicHandler, config: Dict[str, Any]):
	global tracker_worker_handle

	detection_queue = mp.Queue()
	tracked_queue = mp.Queue()

	handler.subscribe('detected', partial(on_detection, detection_queue))

	tracker_worker_process = mp.Process(
		target=tracker_worker,
		args=[config, detection_queue, tracked_queue, update_tracker_ev]
	)

	tracker_worker_process.start()

	tracker_pub_thread = th.Thread(
		target=tracker_pub_worker, 
		args=[handler, tracked_queue]
	)

	tracker_pub_thread.start()

	egoros_config = egoros.Configuration( # This linter is stupid
		tick_rate = 2.0
	)

	return egoros_config

def tick(handler: egoros.TopicHandler):
	update_tracker_ev.set()

def on_detection(queue: mp.Queue, obj: Object):
	queue.put(obj)

def tracker_worker(config: Dict[str, Any], detections_q: mp.Queue[Object], tracked_q: mp.Queue[TrackedObject], update_tracker_ev: Event):
	global should_stop

	tracker = nf.Tracker(
		distance_function='euclidean',
		distance_threshold=config.get('threshold', 10)
	)

	def iterate_queue(queue: mp.Queue[Object]):
		while not queue.empty():
			item = queue.get_nowait()
			yield item

	while not should_stop:
		if update_tracker_ev.wait(timeout=1):
			update_tracker_ev.clear() # Maybe this should be added to the end to account for processing time. But we'll see
			# Map Object to Detection
			detections = [nf.Detection(
				points=np.array([o.position]),
				data=o
			) for o in iterate_queue(detections_q)]

			if len(detections) != 0:
				norfair_tracked = tracker.update(detections)
				mapped_tracked = (
					TrackedObject(
						position=t.get_estimate()[0],
						velocity=t.estimate_velocity,
						size=t.last_detection.data.size
					)
				for t in norfair_tracked)

				for t in mapped_tracked:
					tracked_q.put(t)

				print(f'Tracked {len(norfair_tracked)} objects from {len(detections)} detections')

def tracker_pub_worker(handler: egoros.TopicHandler, queue: mp.Queue[TrackedObject]):
	global should_stop
	while not should_stop:
		try:
			tracked = queue.get(timeout=1)
			handler.publish('tracked', tracked)
		except Empty:
			pass

def on_reload():
	global should_stop
	should_stop = True
