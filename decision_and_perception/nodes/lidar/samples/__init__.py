from __future__ import annotations
from typing import List, Generator

import json
import os.path as pth
import time

from lidar.datamodel import LidarRotation, DataPoint, json_to_datapoints

import logging
log = logging.getLogger('egoros')

CURRENT_PATH = pth.abspath(pth.dirname(__file__))

def fake_data_generator(filename: str = 'linear.json') -> Generator[LidarRotation, None, None]:
    with open(pth.join(CURRENT_PATH, filename), 'r') as file:
        json_data = json.load(file)
        data_points: List[DataPoint] = json_to_datapoints(json_data)

    # Sort data by timestamp
    sorted_data_points = sorted(data_points, key=lambda x: x.timestamp)

    while True:  # Infinite loop
        # Forward iteration
        for rotation in iterate_with_delays(sorted_data_points):
            yield rotation

        # Backward iteration
        for rotation in iterate_with_delays(reversed(sorted_data_points)):
            yield rotation

def iterate_with_delays(data_points: List[DataPoint]) -> Generator[LidarRotation, None, None]:
    previous_timestamp = None
    for data_point in data_points:
        if previous_timestamp is not None:
            time_delta = abs((data_point.timestamp - previous_timestamp).total_seconds())
            time.sleep(time_delta)
        yield data_point.rotation
        previous_timestamp = data_point.timestamp
