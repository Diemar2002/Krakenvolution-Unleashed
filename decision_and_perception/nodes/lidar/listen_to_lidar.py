from __future__ import annotations
from typing import Generator
from .calc_lidar_data import calc_lidar_data
from .datamodel import LidarData, LidarRotation, new_polar_matrix
import numpy as np


import serial

def __listen_to_lidar(port: str) -> Generator[LidarData, None, None]:
    serial_port = serial.Serial(port=port, baudrate=230400, timeout=5.0, bytesize=8, parity='N', stopbits=1)
    packet_length = 4 + (3 * 12) + 4 + 1  # in bytes
    last_byte_was_header = False
    buffer = ""

    while True:
        byte = serial_port.read()
        byte_as_int = int.from_bytes(byte, 'big')

        if byte_as_int == 0x54:
            buffer += byte.hex()
            last_byte_was_header = True
            continue

        if last_byte_was_header and byte_as_int == 0x2c:
            buffer += byte.hex()

            if len(buffer[0:-4]) == packet_length * 2:
                lidar_data = calc_lidar_data(buffer[0:-4])
                yield lidar_data  # Yielding the new lidar_data object

            buffer = ""
        else:
            buffer += byte.hex()

        last_byte_was_header = False
	
# This probably contains more information that it needs. But for now it should be fine
def lidar_measure_generator(port: str) -> Generator[LidarRotation, None, None]:
	packet_generator = __listen_to_lidar(port)

	# State-keeping variables
	speeds = np.array([], dtype=int)
	confidences = np.array([], dtype=float)
	coordinates = new_polar_matrix()  # Assuming this returns an empty matrix appropriate for concatenation.

	combined_angle = 0.0
	for lidar_data in packet_generator:
		combined_angle += min(
			abs(lidar_data.end_angle - (lidar_data.start_angle - 365)),
			abs(lidar_data.end_angle - lidar_data.start_angle)
		)
		if combined_angle >= 360.0:
			# Detected a new rotation
			average_speed = int(np.mean(speeds))
			
			yield LidarRotation(speed=average_speed, 
								confidence=confidences,  # directly using the accumulated array
								coords=coordinates)  # Ensure type
			
			# Reset state-keeping variables
			speeds = np.array([], dtype=int)
			confidences = np.array([], dtype=float)
			coordinates = new_polar_matrix()

			combined_angle = 0.0

		# Accumulate data
		speeds = np.append(speeds, lidar_data.speed)
		confidences = np.append(confidences, lidar_data.confidence)
		# Assuming coords in lidar_data is also a numpy array or can be concatenated.
		coordinates = np.concatenate([coordinates, lidar_data.coords])

