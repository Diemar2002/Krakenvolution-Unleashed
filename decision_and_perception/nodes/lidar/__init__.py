from __future__ import annotations
from typing import Any, Dict, Generator


from .listen_to_lidar import lidar_measure_generator
from .samples import fake_data_generator
from .clustering import DBSCANParams, Object, estimate_objects
from .datamodel import CartessianCoordsMatrix, LidarRotation, polar_to_cartesian, polar_to_cartesian_matrix
from .filter import filter_objs

import egoros
import multiprocessing as mp
import threading as th

import logging
log = logging.getLogger('egoros')

should_stop: bool = False

def init(handler: egoros.TopicHandler, config: Dict[str, Any]):
	# Load data sources
	if config.get('fake', False):
		data_source = fake_data_generator(config.get('fake_datapath', 'linear.json'))
	else:
		if not 'port' in config:
			raise ValueError('The lidar node needs a USB port')
		data_source = lidar_measure_generator(config['port'])

	detected_queue: mp.Queue[Object] = mp.Queue()
	rot_queue: mp.Queue[LidarRotation] = mp.Queue()
	
	dbscan_params = DBSCANParams(
		eps = config.get('eps', 20),
		min_samples = config.get('min_samples', 2)
	)

	# Load process
	detected_pub_thread = th.Thread(
		target=pub_worker,
		args=[detected_queue, handler]
	)

	rotation_pub_thread = th.Thread(
		target=rot_publisher,
		args=[rot_queue, handler]
	)

	process = mp.Process(
		target=process_worker,
		args=[detected_queue, rot_queue, data_source, dbscan_params]
	)

	detected_pub_thread.daemon = True
	process.daemon = True
	detected_pub_thread.start()
	process.start()
	rotation_pub_thread.daemon = True
	rotation_pub_thread.start()

	return egoros.Configuration()

def pub_worker(queue: mp.Queue[Object], handler: egoros.TopicHandler):
	global should_stop
	while not should_stop:
		value = queue.get()
		handler.publish('detected', value)
		
		
def rot_publisher(queue: mp.Queue[LidarRotation], handler: egoros.TopicHandler):
	global should_stop
	while not should_stop:
		value = queue.get()
		# handler.publish('lidar', value)

def process_worker(detected_queue: mp.Queue[Object], rot_queue: mp.Queue[CartessianCoordsMatrix], data_source: Generator[LidarRotation, None, None], dbscan_params: DBSCANParams):
	global should_stop
	for rot in data_source:
		if should_stop:
			break
		rot_queue.put(polar_to_cartesian_matrix(rot.coords))
		tracked = filter_objs(estimate_objects(rot, dbscan_params))
		for obj in tracked:
			detected_queue.put(obj)

def on_reload():
	global should_stop
	should_stop = True
	pass
