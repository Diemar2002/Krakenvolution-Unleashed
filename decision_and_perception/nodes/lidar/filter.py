from __future__ import annotations
from typing import Iterable
import numpy as np
from itertools import filterfalse

from .clustering import Object

MAX_DISTANCE = np.linalg.norm(np.array([2.0, 3.0])) * 100.0
MIN_DISTANCE = 25.0 # cm
MAX_SIZE = 8

def __is_valid(obj: Object):
	distance = np.linalg.norm(obj.position)
	return (distance >= MAX_DISTANCE) or (distance <= MIN_DISTANCE) or (obj.size >= MAX_SIZE)
	

def filter_objs(objs: Iterable[Object]) -> Iterable[Object]:
	return filterfalse(
		__is_valid,
		objs
	)
