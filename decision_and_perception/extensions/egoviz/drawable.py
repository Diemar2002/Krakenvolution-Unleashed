'''
	This file defines the varios shapes that can be used in the configuration.
 	This shapes are created with template objects that store the functions used to instantiate new shapes when a new message is received.

	It follows the next diagram:

 ┌─────────────────┐    ┌─────────────────┐        ┌────────┐
 │                 │    │                 │        │        │
 │  Configuration  ├───►│ Shape templates ├───┬───►│ Shapes │
 │                 │    │                 │   │    │        │
 └─────────────────┘    └─────────────────┘   │    ├────────┤
                                 ▲            │    │        │
        ┌──────────┐             │            ├───►│ Shapes │
        │          │             │            │    │        │
        │ Messages ├─────────────┘            │    ├────────┤
        │          │                          │    │        │
        └──────────┘                          └───►│ Shapes │
                                                   │        │
                                                   └────────┘
'''
from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any, Callable, Dict, List, Optional, Self
from .ui_data import Colors, Point2D
from abc import ABC, abstractmethod

Message = Any # TODO: create actual type for messages

@dataclass(slots=True)
class TimeInfo:
	ttl: Optional[float]
	alive_time: float = 0

@dataclass(slots=True)
class BoundingBox():
	min: Point2D = field(default_factory=Point2D)
	max: Point2D = field(default_factory=Point2D)

def calculate_total_bounding_box(shapes: List[Shape]) -> BoundingBox:
	if not shapes:
		return BoundingBox()  # Return an empty bounding box if no shapes are provided

	# Initialize the min and max coordinates with extremely large/small values
	min_x, max_x = float('inf'), float('-inf')
	min_y, max_y = float('inf'), float('-inf')

	# Iterate over all shapes to find the global min and max x, y
	for shape in shapes:
		shape_bounding_box = shape.shape.bounding()  # Get the bounding box of the shape

		# Update the min and max coordinates
		min_x = min(min_x, shape_bounding_box.min.x)
		max_x = max(max_x, shape_bounding_box.max.x)
		min_y = min(min_y, shape_bounding_box.min.y)
		max_y = max(max_y, shape_bounding_box.max.y)

	# Create a new BoundingBox with the global min and max points
	# No need to convert these to world space as they should already be in it
	return BoundingBox(min=Point2D(min_x, min_y), max=Point2D(max_x, max_y))

class ShapeInstance(ABC):
	@abstractmethod
	def bounding(self) -> BoundingBox:
		pass


#################
# Actual shapes #
#################
@dataclass(slots=True)
class Point(ShapeInstance):
	x: float
	y: float

	def bounding(self) -> BoundingBox:
		point = Point2D(self.x, self.y)
		return BoundingBox(point, point)

@dataclass(slots=True)
class Line(ShapeInstance):
	start_x: float
	start_y: float
	end_x: float
	end_y: float

	def bounding(self) -> BoundingBox:
		return BoundingBox(
			min=Point2D(self.start_x, self.start_y),
			max=Point2D(self.end_x, self.end_y)
		)

@dataclass
class Circle(ShapeInstance):
	x: float
	y: float
	r: float

	def bounding(self) -> BoundingBox:
		min_corner = Point2D(
			self.x - self.r,
			self.y - self.r
		)
		max_corner = Point2D(
			self.x + self.r,
			self.y + self.r
		)
		return BoundingBox(
			min=min_corner,
			max=max_corner
		)


# Templates
KWArgs_t = Dict[str, Any]
FloatFunc = Callable[[Message, KWArgs_t], float]
ColorFunc = Callable[[Message, KWArgs_t], Colors]

YamlStructure = Dict[str, Any]

class ShapeTemplate(ABC):
	@abstractmethod
	def gen_shape(self, msg: Message, **kwargs) -> Shape:
		pass

	@staticmethod
	@abstractmethod
	def from_dict(d: YamlStructure) -> Optional[ShapeTemplate]:
		pass

def callable_from_str(s: str) -> Callable[[Message, KWArgs_t], Any]:
	return lambda msg, args: eval(f'lambda msg: {s}', args, {})(msg)

########################
# Template definitions #
########################
@dataclass(slots=True)
class PointTemplate(ShapeTemplate):
	gen_x: FloatFunc
	gen_y: FloatFunc
	color: ColorFunc
	time: FloatFunc

	def gen_shape(self, msg: Message, **vars) -> Shape:
		return Shape(
			shape = Point(
				x = self.gen_x(msg, vars),
				y = self.gen_y(msg, vars)
			),
			color = self.color(msg, vars),
			time = TimeInfo(ttl=self.time(msg, vars))
		)

	@staticmethod
	def from_dict(d: YamlStructure) -> Optional[ShapeTemplate]:
		try:
			return PointTemplate (
				gen_x = callable_from_str(d['x']),
				gen_y = callable_from_str(d['y']),
				color = callable_from_str(d.get('color', 'Colors.BLUE')),
				time = callable_from_str(d.get('time', '1.0'))
			)
		except KeyError as e:
			print(f'Required key was not present in configuration...\n{e}')
		except SyntaxError as e:
			print(f'Failed to create expression...\n{e}')
			return None

@dataclass(slots=True)
class LineTemplate(ShapeTemplate):
	gen_start_x: FloatFunc
	gen_start_y: FloatFunc
	gen_end_x: FloatFunc
	gen_end_y: FloatFunc
	color: ColorFunc
	time: FloatFunc

	def gen_shape(self, msg: Message, **vars) -> Shape:
		return Shape (
			shape = Line(
				start_x = self.gen_start_x(msg, vars),
				start_y = self.gen_start_y(msg, vars),
				end_x = self.gen_end_x(msg, vars),
				end_y = self.gen_end_y(msg, vars)
			),
			color = self.color(msg, vars),
			time = TimeInfo(ttl=self.time(msg, vars))
		)

	@staticmethod
	def from_dict(d: YamlStructure) -> Optional[ShapeTemplate]:
		try:
			return LineTemplate(
				gen_start_x = callable_from_str(d['start_x']),
				gen_start_y = callable_from_str(d['start_y']),
				gen_end_x = callable_from_str(d['end_x']),
				gen_end_y = callable_from_str(d['end_y']),
				color = callable_from_str(d.get('color', 'Colors.BLUE')),
				time = callable_from_str(d.get('time', '1.0'))
			)
		except KeyError as e:
			print(f'Required key was not present in configuration...\n{e}')
		except SyntaxError as e:
			print(f'Failed to create expression...\n{e}')

@dataclass(slots=True)
class CircleTemplate(ShapeTemplate):
	gen_x: FloatFunc
	gen_y: FloatFunc
	gen_r: FloatFunc
	color: ColorFunc
	time: FloatFunc

	def gen_shape(self, msg: Message, **vars) -> Shape:
		return Shape (
			shape = Circle(
				x = self.gen_x(msg, vars),
				y = self.gen_y(msg, vars),
				r = self.gen_r(msg, vars),
			),
			color = self.color(msg, vars),
			time = TimeInfo(ttl=self.time(msg, vars))
		)

	@staticmethod
	def from_dict(d: YamlStructure) -> Optional[ShapeTemplate]:
		try:
			return CircleTemplate(
				gen_x = callable_from_str(d['x']),
				gen_y = callable_from_str(d['y']),
				gen_r = callable_from_str(d['r']),
				color = callable_from_str(d.get('color', 'Colors.BLUE')),
				time = callable_from_str(d.get('time', '1.0'))
			)
		except KeyError as e:
			print(f'Required key was not present in configuration...\n{e}')
		except SyntaxError as e:
			print(f'Failed to create expression...\n{e}')

ShapeTemplate = PointTemplate | LineTemplate

@dataclass(slots=True)
class Shape:
	shape: ShapeInstance
	time: TimeInfo
	color: Colors
