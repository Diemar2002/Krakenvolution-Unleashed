from __future__ import annotations
from typing import List
import pygame

from ..ui_data import RenderContext, Colors

def draw_info(ctx: RenderContext, lines: List[str]):
    # Constants for styling
    padding = 10
    line_spacing = 5
    border_radius = 5
    font_size = 20
    font_color = Colors.WHITE.value
    background_color = Colors.BLUE.value
    font = pygame.font.Font(None, font_size)

    # Calculate the height and width of the text box
    line_heights = [font.size(line)[1] for line in lines]
    box_height = sum(line_heights) + padding * 2 + line_spacing * (len(lines) - 1)
    box_width = max(font.size(line)[0] for line in lines) + padding * 2

    # Position of the box in the bottom right corner
    screen_width, screen_height = ctx.surface.get_size()
    box_x = screen_width - box_width - padding
    box_y = screen_height - box_height - padding

    # Draw the rectangle
    rect = pygame.Rect(box_x, box_y, box_width, box_height)
    pygame.draw.rect(ctx.surface, background_color, rect, border_radius=border_radius)

    # Draw the text
    y = box_y + padding
    for line in lines:
        text_surface = font.render(line, True, font_color)
        ctx.surface.blit(text_surface, (box_x + padding, y))
        y += font.size(line)[1] + line_spacing
