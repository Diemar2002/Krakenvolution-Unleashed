from __future__ import annotations
from typing import List

import pygame

from ..ui_data import Point2D, RenderContext, scale_to_screen_space
from ..drawable import Circle, Line, Point, Shape

POINT_RADIUS = 10.0
LINE_WIDTH = 3

def draw_drawable(ctx: RenderContext, drawables: List[Shape]):
	# Create an intermediate surface with SRCALPHA
	temp_surface = pygame.Surface(ctx.surface.get_size(), pygame.SRCALPHA)

	for shape in drawables:
		if shape.time.ttl is not None:
			ttlalpha = pygame.math.clamp(shape.time.ttl - shape.time.alive_time, 0, shape.time.ttl) / shape.time.ttl
		else:
			ttlalpha = 1.0

		scale = (ttlalpha / 2.0) + 0.5
		scaled_alpha = int(ttlalpha * 255)

		match shape.shape:
			case Point(x, y):
				pygame.draw.circle(
					surface=temp_surface,
					color=(*shape.color.value, int(255)),
					center=Point2D(x, y).to_screen_space(ctx).to_tuple(),
					radius=POINT_RADIUS / 2.0 * scale
				)
			case Line(start_x, start_y, end_x, end_y):
				pygame.draw.line(
					surface=temp_surface,
					color=(*shape.color.value, scaled_alpha),
					start_pos=Point2D(start_x, start_y).to_screen_space(ctx).to_tuple(),
					end_pos=Point2D(end_x, end_y).to_screen_space(ctx).to_tuple(),
					width=LINE_WIDTH
				)
			case Circle(x, y, r):
				pygame.draw.circle(
					surface=temp_surface,
					color=(*shape.color.value, scaled_alpha),
					center=Point2D(x, y).to_screen_space(ctx).to_tuple(),
					radius=scale_to_screen_space(r, ctx),
					width=LINE_WIDTH
				)

		# Update shapes data
		shape.time.alive_time += ctx.dt

	# Blit the temporary surface onto the main surface
	ctx.surface.blit(temp_surface, (0, 0))
