from __future__ import annotations
import pygame

from ..ui_data import RenderContext, DISCRETIZED_SCALE, Point2D, Colors

def draw_grid(ctx: RenderContext):
	discretized = 10 ** (int(ctx.zoom * DISCRETIZED_SCALE) / DISCRETIZED_SCALE)

	min_corner = Point2D(0, 0).to_world_space(ctx)
	max_corner = Point2D(ctx.surface.get_width(), ctx.surface.get_height()).to_world_space(ctx)

	pos = (min_corner.y // discretized) * discretized
	while pos < max_corner.y:
		color = Colors.BLUE.value if abs(pos) < (discretized / 2) else Colors.LIGHT_GRAY.value
		pygame.draw.line(
			surface=ctx.surface,
			color=color,
			start_pos=Point2D(min_corner.x, pos).to_screen_space(ctx).to_tuple(),
			end_pos=Point2D(max_corner.x, pos).to_screen_space(ctx).to_tuple(),
			width=3
		)
		pos += discretized

	pos = (min_corner.x // discretized) * discretized
	while pos < max_corner.x:
		color = Colors.BLUE.value if abs(pos) < (discretized / 2) else Colors.LIGHT_GRAY.value
		pygame.draw.line(
			surface=ctx.surface,
			color=color,
			start_pos=Point2D(pos, min_corner.y).to_screen_space(ctx).to_tuple(),
			end_pos=Point2D(pos, max_corner.y).to_screen_space(ctx).to_tuple(),
			width=3
		)
		pos += discretized
