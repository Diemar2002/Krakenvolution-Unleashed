import argparse

from typing import Dict, List, Optional
from pygame import threads
import yaml
from time import sleep

from .parser import TopicGenerator, templates_from_yaml

from .drawable import Shape
from .ui_data import Colors

import pygame as pg

from . import ui

CLEANER_INTERVAL = 1.0

parser = argparse.ArgumentParser()

parser.add_argument (
	'--config',
	action='store',
	help='Configuration file'
)

args = parser.parse_args()


def templates_from_file(path: str) -> Optional[dict[str, Dict[str, TopicGenerator]]]:
	with open(path, 'r') as file:
		info = yaml.load(file, yaml.SafeLoader)
		return templates_from_yaml(info)

shapes: List[Shape] = []
CONFIG_PATH = args.config

templates = templates_from_file(CONFIG_PATH)
if templates is not None:
	def writer_worker():
		args = {
			'a': 2.0,
			'b': 2.0,
			'Colors': Colors
		}
		while True:
			test = templates['test']['points']
			points = [
				[1, 1],
				[1, -1],
				[-1, -1],
				[-1, 1]
			]
			generated_shapes = test.gen_shapes(msg={
				'coords': points
			}, **args)
			shapes.extend(generated_shapes)
			sleep(1)

	writer_worker_handler = threads.Thread(target=writer_worker)
	writer_worker_handler.daemon = True
	writer_worker_handler.start()

	# Actual implementation
	def on_key(key: int):
		global templates
		if key == pg.K_r:
			try:
				templates = templates_from_file(CONFIG_PATH)
				print('Configuration reloaded')
			except Exception as e:
				print(f'Failed to load configuration from {CONFIG_PATH}\n\t{e}')

	# Cleaner thread
	def cleaner_worker():
		while True:
			shapes[:] = [shape for shape in shapes if (
				(shape.time.ttl > shape.time.alive_time) 
					if (shape.time.ttl is not None) 
				else True)
			]
			sleep(CLEANER_INTERVAL)

	cleaner_thread = threads.Thread(target=cleaner_worker)
	cleaner_thread.daemon = True
	cleaner_thread.start()
	# Load user interface
	ui.run_UI(shapes, on_key)
