from __future__ import annotations
from functools import partial
from typing import Optional, Dict

import imp

import pygame
try:
	imp.find_module('egoros')
	egoros_found: bool = True
except ImportError:
	egoros_found: bool = False

if egoros_found:
	import logging
	from typing import Dict, List
	log = logging.getLogger('egoros')

	CLEANER_INTERVAL = 0.2

	from egoros.mod.extension_interface import TopicName_t, NodeName_t
	from time import sleep

	from .ui import run_UI, Colors
	from .drawable import Shape
	from .parser import TopicGenerator, templates_from_yaml

	import yaml
	import threading as th

	shapes: List[Shape] = []
	templates: Dict[str, Dict[str, TopicGenerator]] = {}

	shape_gen_ctx = {
		'Colors': Colors
	}

	def templates_from_file(path: str) -> Optional[Dict[str, Dict[str, TopicGenerator]]]:
		with open(path, 'r') as file:
			info = yaml.load(file, yaml.SafeLoader)
			return templates_from_yaml(info)

	def on_key(key: int, path: Optional[str], templates: Dict):
		if path is not None:
			if key == pygame.K_r:
				log.info('Reloading EgoViz configuration')
				new_templates = templates_from_file(path)
				if new_templates is not None:
					templates.clear()
					templates.update(new_templates)
				else:
					log.warning(f'Failed to load configuration from {path}')
		else:
			log.warning('Can not reload configuration because config_path has not been specified')

	def init(pub_function, config):
		global templates

		config_path = config.get('config_path', None)
		if config_path is not None:
			generated = templates_from_file(config_path)
			if generated is not None:
				templates = generated
			else:
				templates = templates_from_yaml(config)
		else:
			templates = templates_from_yaml(config)

		ui_thread = th.Thread(
			target=run_UI,
			args=[shapes, partial(on_key, templates=templates, path=config_path)],
			kwargs={
				'framerate': config.get('framerate', None)
			}
		)
		ui_thread.daemon = True
		ui_thread.start()

		cleaner_thread = th.Thread(target=cleaner_worker, args=[shapes])
		cleaner_thread.daemon = True
		cleaner_thread.start()


	def cleaner_worker(shapes: List[Shape]):
		while True:
			shapes[:] = [shape for shape in shapes if (
				(shape.time.ttl > shape.time.alive_time) 
					if (shape.time.ttl is not None) 
				else True)
			]
			sleep(CLEANER_INTERVAL)

	def on_publish(received_topic: TopicName_t, msg):
		global shapes
		for display_name, display in templates.items():
			for topic, generator in display.items():
				if topic == received_topic:
					generated_shapes = generator.gen_shapes(msg, **shape_gen_ctx)
					shapes.extend(generated_shapes)

	def on_subscribe(node_name: NodeName_t, topic_name: TopicName_t):
		pass

	def on_node_state_change(node_name: NodeName_t, state):
		pass

	def on_orchestrator_state_change(state):
		pass


