from __future__ import annotations
from typing import Tuple
from dataclasses import dataclass
import pygame

DEFAULT_SCALE = 10 # m/vmin
SCROLL_SCALE = 10.0
DISCRETIZED_SCALE = 2.5

import enum
class Colors(enum.Enum):
	WHITE = (255, 255, 255)
	BLACK = (18, 18, 18)
	DARK_GRAY = (44, 50, 61)        # bar-back: #2c323d
	GRAY_BLUE = (62, 68, 82)        # bar-unselected: #3e4452
	BLUE = (79, 139, 188)      # bar-blue: #4f8bbc
	GREEN = (152, 195, 121)    # bar-green: #98c379
	PURPLE = (177, 123, 199)   # bar-purple: #b17bc7
	ORANGE = (208, 113, 122)    # bar-redorange: #d0717a
	PINK = (215, 7, 118)      # highlight: #d70976
	LIGHT_GRAY = (216, 222, 233)    # text: #d8dee9

@dataclass(slots=True)
class RenderContext:
	surface: pygame.Surface
	zoom: float
	camera_offset: Point2D
	dt: float

@dataclass(slots=True)
class Point2D:
	x: float = 0.0
	y: float = 0.0

	def to_screen_space(self, ctx: RenderContext) -> Point2D:
		width = ctx.surface.get_width()
		height = ctx.surface.get_height()
		
		vmin = min(width, height)
		true_zoom = 10 ** ctx.zoom
		current_multiplier = vmin / (DEFAULT_SCALE * true_zoom)
		return Point2D(
			x = self.x * current_multiplier + (width / 2) + ctx.camera_offset.x,
			y = self.y * current_multiplier + (height / 2) + ctx.camera_offset.y
		)

	def to_world_space(self, ctx: RenderContext) -> 'Point2D':
		width = ctx.surface.get_width()
		height = ctx.surface.get_height()

		vmin = min(width, height)
		true_zoom = 10 ** ctx.zoom
		current_multiplier = vmin / (DEFAULT_SCALE * true_zoom)

		# Reverse the screen space transformation
		return Point2D(
			x = (self.x - (width / 2) - ctx.camera_offset.x) / current_multiplier,
			y = (self.y - (height / 2) - ctx.camera_offset.y) / current_multiplier
		)
		
	def to_tuple(self) -> Tuple[float, float]:
		return (self.x, self.y)

def scale_to_screen_space(size: float, ctx: RenderContext) -> float:
    width = ctx.surface.get_width()
    height = ctx.surface.get_height()
    
    vmin = min(width, height)
    true_zoom = 10 ** ctx.zoom
    current_multiplier = vmin / (DEFAULT_SCALE * true_zoom)

    return size * current_multiplier
