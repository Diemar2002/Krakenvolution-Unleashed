from __future__ import annotations
from typing import Callable, List, Tuple
import pygame

import numpy as np


from .drawable import Shape

from .ui_data import RenderContext, Point2D, SCROLL_SCALE, Colors
from .ui_elements.grid import draw_grid
from .ui_elements.info import draw_info
from .ui_elements.drawables import draw_drawable

def get_vmin(ctx: RenderContext):
	return min(ctx.surface.get_width(), ctx.surface.get_height())


def run_UI(shapes: List[Shape], on_key_pressed: Callable[[int], None], resolution: Tuple[int, int] = (800, 600), framerate: float = 60.0):
	pygame.init()
	screen = pygame.display.set_mode(resolution, pygame.SRCALPHA)
	clock = pygame.time.Clock()
	running = True

	render_context = RenderContext(
		surface=screen,
		zoom=0.0,
		camera_offset=Point2D(0, 0),
		dt = clock.get_time() / 1000.0
	)

	while running:
		keys = pygame.key.get_pressed()
		mouse = pygame.mouse.get_pos()
		mouse_world = Point2D(mouse[0], mouse[1]).to_world_space(render_context)		

		for evt in pygame.event.get():
			if evt.type == pygame.QUIT:
				running = False
			elif evt.type == pygame.KEYDOWN:
				on_key_pressed(evt.key)
			elif evt.type == pygame.MOUSEWHEEL:
				if keys[pygame.K_LCTRL]:
						# Step 1: Get Mouse Position
					mouse_x, mouse_y = pygame.mouse.get_pos()
					mouse_screen_space = Point2D(mouse_x, mouse_y)

					mouse_world_space_pre_zoom = mouse_screen_space.to_world_space(render_context)

					if keys[pygame.K_LCTRL]:
						render_context.zoom += evt.y * 0.1

					mouse_screen_space_post_zoom = mouse_world_space_pre_zoom.to_screen_space(render_context)

					render_context.camera_offset.x += mouse_screen_space.x - mouse_screen_space_post_zoom.x
					render_context.camera_offset.y += mouse_screen_space.y - mouse_screen_space_post_zoom.y
				else:
					render_context.camera_offset.x += -evt.x * SCROLL_SCALE
					render_context.camera_offset.y += evt.y * SCROLL_SCALE
		
		screen.fill(Colors.WHITE.value)

		draw_grid(render_context)
		draw_drawable(render_context, shapes)
		draw_info(render_context, [
			f'Pointer at ({mouse_world.x:.3f}, {mouse_world.y:.3f})',
			f'Distance to center: {np.linalg.norm(np.array(mouse_world.to_tuple())):.3f}',
			f'{len(shapes)} shapes {int(clock.get_fps())} FPS'
		])

		pygame.display.flip()
		clock.tick(framerate)
		render_context.dt = clock.get_time() / 1000.0



