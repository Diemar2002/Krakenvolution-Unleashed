from __future__ import annotations
from typing import Callable, Iterable, List, Dict, Optional, Type, cast
from dataclasses import dataclass

from .drawable import CircleTemplate, LineTemplate, Message, ShapeTemplate, Shape, YamlStructure, PointTemplate

ListGenFunction = Callable[[Message], Iterable]
@dataclass(slots=True)
class TopicGenerator:
	generators: List[ShapeTemplate]
	loop: Optional[ListGenFunction]

	def gen_shapes(self, msg: Message, **args) -> List[Shape]:
		if self.loop is None:
			return [gen.gen_shape(msg, **args) for gen in self.generators]
		else:
			shapes: List[Shape] = []
			for item in self.loop(msg):
				gen_args = {'item': item, **args}
				shapes.extend([gen.gen_shape(msg, **gen_args) for gen in self.generators])
			return shapes

TYPE_MAPPING: Dict[str, Type[ShapeTemplate]] = {
	'point': PointTemplate,
	'line': LineTemplate,
	'circle': CircleTemplate
}

def templates_from_yaml(struct: YamlStructure):
	def from_yaml_to_generator(struct: YamlStructure) -> Optional[ShapeTemplate]:
		t: str = struct.get('type', None)
		if t is not None:
			# Check types
			if t in TYPE_MAPPING:
				return TYPE_MAPPING[t].from_dict(struct)
			else:
				print(f'{t} is not a valid display type')
				return None

	templates: Dict[str, Dict[str, TopicGenerator]] = {}
	displays = struct['displays']
	for display_name, display in displays.items():
		templates[display_name] = {}
		for topic, drawables in displays[display_name].items():
			loop: Optional[ListGenFunction] = None
			for drawable in drawables:
				if 'loop' in drawable:
					loop = eval(f'lambda msg: {drawable["loop"]}')

			templates[display_name][topic] = TopicGenerator(
				generators=cast(List[ShapeTemplate], list(
				filter(
					lambda elem: elem is not None,
					[from_yaml_to_generator(drawable) for drawable in drawables]
				))),
				loop=loop
			)

	return templates
