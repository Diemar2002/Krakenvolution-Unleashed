from setuptools import setup, find_packages

setup (
	name = 'egoros',
	version = '0.0.1',
	packages = find_packages(),
	entry_points={
		 "console_scripts": [
			"egoros = egoros.__main__:main",
		 ],
	},
	install_requires = [
		'coloredlogs==15.0.1',
		'rich==13.4.2',
		'watchdog==3.0.0',
		'rich_argparse==1.2.0',
		'pyyaml==6.0.1'
	],
	author = 'Diego Morales Román',
	author_email = 'moralesromandiego@gmail.com',
	description = 'Basic message broker for python (similar to ROS)',
	include_package_data = True,
	package_data= {
		'egoros': [
			'cli/splash/splash.txt',
			'docs/**'
		]
	}
)
