#include "ArduinoJson/Document/StaticJsonDocument.hpp"
#include "egoros.hpp"
#include <cstddef>
#include <string.h>

namespace egoros {
	template<typename T, size_t doc_size>
	void publish(const char* topic, const T&& value) {
		auto serialized = std::to_string(value);

		__impl::send_pub_header();
		on_write(serialized.c_str(), serialized.size());
		__impl::send_tail();
	}
}
