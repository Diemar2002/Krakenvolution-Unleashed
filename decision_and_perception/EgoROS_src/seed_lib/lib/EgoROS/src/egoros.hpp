#pragma once
#include "Print.h"
#include <string.h>
#include <ArduinoJson.h>
#include <functional>

namespace egoros {
	// using Callback_t = void(*)(const JsonDocument&);
	using Callback_t = std::function<void(const JsonVariant)>;
	void on_write(const char* str, size_t len); // Has to be defined by the user
	void on_read(const char* str, size_t len);

	void publish(const char* topic, const JsonDocument&);
	template<typename T, size_t doc_size = 200>
	void publish(const char* topic, const T&& value);
	void publish(const char* topic, const char* value);

	void subscribe(const char* topic, const Callback_t&& callback);

	namespace __impl {
		void send_pub_header();
		void send_tail();
		class SerialWriter : public Print {
			public:
			size_t write(uint8_t) override;
		};
	}
}

#include "egoros.tcc"
