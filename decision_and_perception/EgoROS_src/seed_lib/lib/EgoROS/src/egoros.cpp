#include "egoros.hpp"

#include <map>
#include <vector>
#include <cstring>

using namespace egoros;

void egoros::publish(const char* topic, const JsonDocument& doc) {
	__impl::send_pub_header();
	auto serializer = __impl::SerialWriter{};
	serializeJson(doc, serializer);
	__impl::send_tail();
}


void egoros::publish(const char* topic, const char* value) {
	__impl::send_pub_header();
	on_write("\"", 1);
	on_write(value, strlen(value));
	on_write("\"", 1);
	__impl::send_tail();
}

void __impl::send_pub_header() {
	on_write("{\"action\":\"publish\",\"value\":", 28);
}

void __impl::send_tail() {
	on_write("}\n", 2);
}

size_t __impl::SerialWriter::write(uint8_t value) {
	char buff = value;
	egoros::on_write(&buff, 1);
	return 1;
}

// Subscription store and subscription management
static std::map<std::string, std::vector<Callback_t>> subscriptions;

void egoros::subscribe(const char *topic, const Callback_t &&callback) {
  auto it = subscriptions.find(topic);
  
  if (it != subscriptions.end()) {
    it->second.push_back(std::move(callback));
  } else {
    subscriptions.insert({topic, std::vector<Callback_t>{std::move(callback)}});
  }

  // Send subscription data over the wire
  on_write("{\"action\":\"subscribe\",\"topic\":\"", 31);
  on_write(topic, strlen(topic));
  on_write("\"}", 2);
}

// NOTE: This was chatGPTed so I should look at this later. Too late now :(
void egoros::on_read(const char *str, size_t len) {
	static std::string buffer;
	buffer.append(str, len);

	size_t pos;
	while ((pos = buffer.find('\n')) != std::string::npos) {
		std::string message = buffer.substr(0, pos);

		buffer.erase(0, pos + 1);

		StaticJsonDocument<512> doc;
		DeserializationError err = deserializeJson(doc, message);

		if (!err && doc.containsKey("topic") && doc.containsKey("value")) {
			std::string topic = doc["topic"].as<std::string>();

			if (subscriptions.find(topic) != subscriptions.end()) {
				JsonVariant value = doc["value"];

				for (const auto& cb : subscriptions[topic]) {
					cb(value);
				}
			}
		}
	}
}
