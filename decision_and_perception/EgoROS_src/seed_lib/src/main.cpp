#include <Arduino.h>
#include <egoros.hpp>
#include <WiFi.h>

void callback(const JsonVariant doc) {
	Serial.println("Received document");
}

void setup() {
	Serial.begin(115200);
	WiFiClient client;
	delay(1000);
	egoros::subscribe("topic", callback);
}

void egoros::on_write(const char *str, size_t len) {
	Serial.write(str, len);
}

void loop() {
	while (Serial.available()) {
		auto value = Serial.readString();
		Serial.println(value);
		egoros::on_read(value.c_str(), value.length());
	}
}
