from __future__ import annotations

def main():
	# Setup logging
	from .mod.logging import configure_logger
	import logging

	log = logging.getLogger('egoros')

	configure_logger(
		 package_name='egoros',
		 log_file='egoros.log'
	)

	# Import parsers
	from .cli import parser

	# Print banner
	from .cli.splash import splash
	splash()

	parser.parse()

if __name__ == '__main__':
	main()
