# CLI
- Stop program if a node crashes while hot-reloading is off

# Configuration
All configuration is located in a YAML file with the following  structure:

nodes:
    lidar_reader:
        flag: value
        flag: value
    navigation:
        grid_x: 10
        grid_y: 20
extensions:
    network:
        addr: 127.0.0.1
        port: 8080

- Change configuration dataclass to not have a name. The name of the node will be taken from the filename
- Add configuration file support

# Extensions
Extensions should have subparsers available (use the same system that's already present
## Example of extension CLI usage
python3 -m egoros --cfg-file config.toml launch nodes --enable-hot-reolading
## Example of configuration file
The configuration file will be a toml file. Every [entry] will be an extension that will be launched

```toml
[network]
port=8080
addr=127.0.0.1
```
## Default extensions
- Network extension


         
