from __future__ import annotations

import logging
log = logging.getLogger('egoros')

import egoros 

def init(pub_function, config):
	log.info(f'Loaded test extension with config:\n{config}')

def on_publish(topic_name: TopicName_t, msg):
	log.info(f'{msg} -> {topic_name}')

def on_subscribe(node_name: NodeName_t, topic_name: TopicName_t):
	log.info(f'"{node_name}" subscribed to "{topic_name}"')

def on_node_state_change(node_name: NodeName_t, state):
	log.info(f'node "{node_name}" changed state to {state}')

def on_orchestrator_state_change(state):
	log.info(f'Orchestrator changed state to {state}')


