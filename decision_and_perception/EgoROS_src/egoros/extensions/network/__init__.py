import logging
from typing import Any, Callable, Dict

from egoros.mod.extension_interface import NodeName_t, TopicName_t
import functools as ft

log = logging.getLogger('egoros')

from .actions import from_json, SubscribeInstruction, PublishInstruction, Instruction
from .networking import Disconnection, launch_server
from .connection_store import ConnectionStore

import socket as sock

store = ConnectionStore()

def on_instruction(publish_function: Callable[[str, Any], None], instruction: Instruction | Disconnection, conn: sock.socket):
	if isinstance(instruction, SubscribeInstruction):
		store.subscribe(instruction.topic, conn)
	elif isinstance(instruction, PublishInstruction):
		publish_function(instruction.topic, instruction.data)
	elif isinstance(instruction, Disconnection):
		store.ubsubscribe(conn)

def init(publish_function: Callable[[str, Any], None], config: Dict[str, Any]):
	log.info(f'Loaded networking extension with config:\n{config}')
	addr: str = config.get('addr', '127.0.0.1')
	port: int = config.get('port', 8080)
	server_thread = launch_server(addr, port, ft.partial(on_instruction, publish_function))


def on_publish(topic_name: TopicName_t, msg):
	store.pub(topic_name, msg)

def on_subscribe(node_name: NodeName_t, topic_name: TopicName_t):
	return
	log.info(f'"{node_name}" subscribed to "{topic_name}"')

def on_node_state_change(node_name: NodeName_t, state):
	return
	log.info(f'node "{node_name}" changed state to {state}')

def on_orchestrator_state_change(state):
	return
	log.info(f'Orchestrator changed state to {state}')



