from __future__ import annotations
from typing import Any, Dict, List, Set, Tuple

import logging 
log = logging.getLogger('egoros')

Addr = Tuple[str, int]

import socket as sock
import json

class ConnectionStore:
	subscriptions: Dict[str, Dict[Addr, sock.socket]] = {}
	
	def subscribe(self, topic: str, conn: sock.socket):
		if not topic in self.subscriptions:
			self.subscriptions[topic] = {} 

		self.subscriptions[topic][conn.getsockname()] = conn

	def ubsubscribe(self, addr: Addr):
		for topic, s in self.subscriptions.items():
			if addr in s:
				s.pop(addr)

	def pub(self, topic: str, value: Any):
		if topic in self.subscriptions:
			try:
				sendable = json.dumps({
					"topic": topic,
					"value": value
				}).encode()

				dic = self.subscriptions[topic]
				for conn in dic.values():
					try:
						conn.send(sendable)
					except sock.error as e:  # Catching OSError or a more specific error if known
						pass

			except TypeError:
				log.warning(f'Value: {value} can not be serialized to JSON')
			except sock.error as e:
				log.warning(f'Failed to send data to a socket\nException: {e}')
