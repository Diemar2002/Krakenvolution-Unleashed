from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Dict
import json

@dataclass
class PublishInstruction:
	topic: str
	data: Any

@dataclass
class SubscribeInstruction:
	topic: str

Instruction = PublishInstruction | SubscribeInstruction

def load_from_publish(json_data: Dict[str, Any]) -> PublishInstruction:
	if not ('topic' in json_data and 'data' in json_data):
		raise ValueError(f'{json_data} does not provide either "topic" or "data" keys')
	else:
		return PublishInstruction(
			topic=json_data['topic'],
			data=json_data['data']
		)

def load_from_subscribe(json_data: Dict[str, Any]) -> SubscribeInstruction:
	if not ('topic' in json_data):
		raise ValueError(f'{json_data} does not contain a "topic" key')
	else:
		return SubscribeInstruction(
			topic=json_data['topic']
		)

INSTRUCTIONS: Dict[str, Callable[[Dict[str, Any]], Instruction]] = {
	'publish': load_from_publish,
	'subscribe': load_from_subscribe
}

def from_json(data: str) -> Instruction:
	json_data: Dict[str, Any] = json.loads(data)
	# Check validity
	if not 'action' in json_data:
		raise ValueError(f'Json data "{json_data}" does not contain an action')
	else:
		action = json_data['action']
		if not action in INSTRUCTIONS:
			raise ValueError(f'Action {action} was not expected, please use one of {list(INSTRUCTIONS.keys())}')
		else:
			return INSTRUCTIONS[action](json_data)
