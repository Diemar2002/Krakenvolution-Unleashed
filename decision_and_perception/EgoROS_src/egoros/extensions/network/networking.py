import socket as sock
import threading as th
import logging

from json.decoder import JSONDecodeError
from typing import Any, Callable

log = logging.getLogger('egoros')
from .actions import Instruction, from_json
from .connection_store import Addr

from dataclasses import dataclass

@dataclass
class Disconnection:
	pass

Topic_t = str
OnInstructionCallback_t = Callable[[Instruction | Disconnection, sock.socket], None]

def launch_server(addr: str, port: int, on_instruction: OnInstructionCallback_t) -> th.Thread:
	s = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
	s.setsockopt(sock.SOL_SOCKET, sock.SO_REUSEADDR, 1)
	s.bind((addr, port))
	s.listen()

	def accepter_worker(s: sock.socket):
		while True:
			try:
				conn, addr = s.accept()
				thread = th.Thread(target=reader_worker, args=(conn, addr), daemon=True)
				thread.start()
			except sock.error as e:
				log.error(f'Socket error: {e}')
				break  # Exit the loop on socket error

	def reader_worker(conn: sock.socket, addr):
		log.info(f'Connected to {addr}')
		try:
			while True:
				data = conn.recv(4096)
				if not data:
					break  # Exit the loop if connection is closed

				try:
					instruction = from_json(data.decode())
					log.info(f'Instruction from {addr}: {instruction}')
					on_instruction(instruction, conn)
				except ValueError as e:
					if type(e) == JSONDecodeError:
						log.warning(f'String "{data.decode()}" is not valid JSON\nReason: {e}')
					else:
						log.warning(f'Failed to use JSON data:\n{e}')
		except sock.error as e:
			log.error(f'Socket error: {e}')
		finally:
			log.info(f'{addr} disconnected')
			on_instruction(Disconnection(), conn)
			conn.close()  # Manually close conn when done or on error

	accepter_thread = th.Thread(target=accepter_worker, args=(s,), daemon=True)
	accepter_thread.start()
	return accepter_thread  # Remember to close `s` when you're done with it
