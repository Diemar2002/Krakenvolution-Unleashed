from argparse import _SubParsersAction
from typing import Any

import logging
log = logging.getLogger('egoros')

NAME = 'docs'

def append_to_parser(subparser: _SubParsersAction, formatter):
    docs_subparser = subparser.add_parser('docs', formatter_class=formatter)
    docs_subparser.add_argument(
        '--tree', '-t',
        action='store_true',
        help='Prints the available docs as a file tree'
    )

    docs_subparser.add_argument(
        'path',
        help='Docs path',
        nargs='?'
    )

from ...docs import print_tree, print_docs

def process(args: Any):
    if args.tree:
        print_tree()
        exit(0)

    if args.path != None:
        try:
            print_docs(args.path)
        except FileNotFoundError:
            log.error(f'''
    "{args.path}" was not found on the docs path.
    Example path: path/to/docs 
    Available docs:
                      ''')
            print_tree()
