from argparse import _SubParsersAction
from typing import Any, Dict, List, Optional

import os.path
import os
import logging

import threading as th

from ...mod.extension_interface import Extension, ExtensionInterface
from ...mod.extensions import load_extensions_from_path
from ...mod.node_handler import NodeState
from ...mod.topic.topic import Message

from ...mod.dirtools import scan_folder_for_nodes
from ...mod.orchestrator import Orchestrator, OrchestratorStatus

log = logging.getLogger('egoros')

EXTENSIONS_FOLDER = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', 'extensions')

NAME = 'launch'

def append_to_parser(subparser: _SubParsersAction, formatter):
	launch_subparser = subparser.add_parser('launch', formatter_class=formatter)

	launch_subparser.add_argument(
		'path',
		help='Path to a folder where the nodes are located',
		nargs='+'
	)

	launch_subparser.add_argument(
		'--enable-hot-reload',
		help='Enables hot reloading for all nodes',
		action='store_true'
	)

	launch_subparser.add_argument(
		'--crash-if-node-fails',
		help='Forces the program to exit if a node crashes (even if hot reloading is enabled)',
		action='store_true'
	)

	launch_subparser.add_argument(
		'--config', '-c',
		help='Loads configuration from the specified file',
		action='store'
	)

	launch_subparser.add_argument(
		'--extensions-folder',
		help='Specifies another folder to load extensions from',
		action='store'
	)

def process(args: Any):
	# Load configuration
	extensions_config: Optional[Dict[str, Any]] = {}
	nodes_config: Dict[str, Any] = {}
	if args.config is not None:
		log.info(f'Loading configuration from {args.config}')
		import yaml
		with open(args.config, 'rt') as file:
			# This should be able to run python code for the configuration
			# Although it shouldn't be used. Wink, wink
			read = yaml.unsafe_load(file)
			extensions_config = read.get('extensions', {})
			nodes_config = read.get('nodes', {})
	
	log.info(f'Extensions configuration: {extensions_config}')
	log.info(f'Nodes configuration: {nodes_config}')

	# Check path
	if not all([os.path.isdir(filename) for filename in args.path]):
		log.error(f'''
	Not all provided paths are folders!
		 {args.path}
							''')
		exit(1)

	# Scan directories for nodes
	node_files: List[str] = []
	for dir in args.path:
		node_files.extend(scan_folder_for_nodes(dir))

	log.info(f'Found the following nodes {node_files}')

	orchestrator = Orchestrator(nodes=node_files)
	stop_event = th.Event()

	# Load extensionslaunc
	if extensions_config is not None:
		extensions_names = '\t'.join(extensions_config.keys())
		log.info(f'Trying to load extensions:\n\t{extensions_names}')

		extensions = load_extensions_from_path(EXTENSIONS_FOLDER)
		# Load custom extensions
		if args.extensions_folder is not None:
			extra_extensions = load_extensions_from_path(args.extensions_folder)
			extensions = {**extensions, **extra_extensions}

		log.info('Initializing extensions...')
		for name, config in extensions_config.items():
			log.info(f'Initializing "{name}" with {config}')
			if not name in extensions:
				log.warning(f'''
	Failed to initialize extension "{name}" as it's not either in the default extensions folder or the custom one.
		If the extension is custom ensure that the --extensions-folder flag is set to it's folder
				''')
			else:
				orchestrator.hook_extension(extensions[name])
				extensions[name].interface.init_extension(orchestrator.publish, config)
	else:
		log.warning('No extensions configuration was found, so no extension will be loaded')

	def on_orchestrator_state_change(state: OrchestratorStatus):
		# Check if the program should crash
		if (not args.enable_hot_reload or args.crash_if_node_fails) and state == OrchestratorStatus.NODES_CRASHED:
			log.critical('''
Some nodes crashed and hot reloading is disabled. The program will crash when
a node crashes only if hot reloading is disabled. If hot reloading is enabled
(--enable-hot-reolad) then the program will still run. To disable this
behaviour use the flag (--crash-if-node-fails).
			''')
			stop_event.set()

	extension = Extension (
		name='egoros',
		interface=ExtensionInterface (
			on_publish=None,
			on_subscribe=None,
			on_orchestrator_state_change=on_orchestrator_state_change,
			on_node_state_change=None,
			init_extension=lambda *_: None
		)
	)

	if args.enable_hot_reload:
		log.info('Hot reloading enabled')
		orchestrator.enable_hot_reloading()

	orchestrator.hook_extension(extension)

	try:
		orchestrator.start(config=nodes_config)
		try:
			stop_event.wait()
		except KeyboardInterrupt:
			log.info('Exiting...')
	finally:
		orchestrator.stop()

	log.info('Goodbye ﮧ')
