import argparse
import rich_argparse
import os.path
import importlib

from typing import Dict, Callable, Any

CURRENT_PATH = os.path.abspath(os.path.dirname(__file__))
PROCESSORS: Dict[str, Callable[[Any], None]] = {}

parser = argparse.ArgumentParser(
    prog='EgoROS',
    description='''
This command line tool does stuff. :3
    
    ''',
    formatter_class=rich_argparse.RichHelpFormatter
)


# Add subparsers
subparser = parser.add_subparsers(
    dest='subparser',
    required=False,
)

for entry in os.scandir(CURRENT_PATH):
    if entry.is_file() and entry.name != '__init__.py':
        # Import module
        full_modname = f'.{entry.name.removesuffix(".py")}'
        mod = importlib.import_module(full_modname, package=__package__)
        # 
        mod.append_to_parser(subparser, rich_argparse.RichHelpFormatter)
        PROCESSORS[mod.NAME] = mod.process

def parse():
    global parser
    args = parser.parse_args()
    if args.subparser != None:
        PROCESSORS[args.subparser](args)
