from __future__ import annotations
from typing import Any, Callable, Dict, List, Optional, Type
from dataclasses import dataclass, field
import inspect
import typing

from .topic.topic import TopicCallback_t

import logging
log = logging.getLogger('egoros')

@dataclass
class SubscriptionGroup:
	"""
   A class representing a group of subscriptions.

	Attributes:
		callbacks (List[TopicCallback_t]): Callbacks that will be invoked when a new message for the topic is received
		inferred_type (Optional[Type]): The inferred type of the subscription group. 
			The inference can occur whenever a concise type for the topic can be seen.
				- Publish: the type is borrowed from the sent message
				- Subscribe: the type is borrowed from the annotation of the function if present
			When a topic's callback has been established, any contradiction, either in publish or subscribe will raise a ValueError
	"""
	callbacks: List[TopicCallback_t] = field(default_factory=list)
	inferred_type: Optional[Type] = None

class SubscriptionStore:
	"""
	A class that acts like a message bus

	Attributes:
		on_subscription (Optional[Callable[[str, TopicCallback_t]): optional
			callback that will be called whenever a subscription is issued if
			defined.
		on_publish (Optional[TopicCallback_t]): optional callback that will be
			called whenever a value is publish in a topic. CURRENTLY NOT IMPLEMENTED

	Methods:
		reset(): clears all subscriptions and topics
		subscribe(str, TopicCallback_t): adds a callback subscription to the store
		publish(str, Any): publishes a value to the message bus
	"""
	def __init__(self) -> None:
		"""
		Initializes the store
		"""
		self.__subscriptions: Dict[str, SubscriptionGroup] = {}

		# Callbacks
		self.on_subscription: Optional[Callable[[str, TopicCallback_t], None]] = None
		# self.on_publish: Optional[TopicCallback_t] = None

	@staticmethod
	def __set_inferred_type(topic: str, group: SubscriptionGroup, type: Type):
		log.debug(f'Inferred type "{type}" for topic "{topic}"')
		group.inferred_type = type

	def reset(self):
		"""
		Clears all subscriptions: useful for when the nodes resets
		"""
		self.__subscriptions = {}

	def subscribe(self, topic: str, callback: TopicCallback_t):
		"""
		Adds a new subscription to the store, tries to assign the topic's message type if possible 

		Args:
			topic (str): name of the topic that's being subscribed
			callback (TopicCallback_t): callback that will be executed when a message to the specified topic is issued
		Raises:
			ValueError: if the inferred topic type from the callback is different from the current one
		"""
		if not topic in self.__subscriptions:
			group = SubscriptionGroup()
			self.__subscriptions[topic] = group
		else:
			group = self.__subscriptions[topic]


		# Get hints from callback
		signature = inspect.signature(callback)
		arg = list(signature.parameters.values())[0] # Has to have one parameter

		if arg.annotation != inspect._empty:
			arg_types = typing.get_args(arg.annotation)
			
			if arg_types:
				arg_t = arg_types[0]
				if group.inferred_type is None:
					self.__set_inferred_type(topic, group, arg_t)
				elif arg_t != group.inferred_type:
					raise ValueError(f'Topic "{topic}" has already been inferred to be of type "{group.inferred_type}". ')
			else:
				self.__subscriptions[topic].callbacks.append(callback)
				if self.on_subscription != None:
					self.on_subscription(topic, callback)


	def publish(self, topic: str, value: Any):
		"""
		Adds a value to the message bus

		Args:
			topic (str): topic to publish the message into
			value (Any): value that will be published
		Raises:
			ValueError: if the type of the message is different from the inferred type of the topic
		"""
		group: Optional[SubscriptionGroup] = self.__subscriptions.get(topic, None)

		if group != None:
			# Type check message
			if group.inferred_type is None:
				self.__set_inferred_type(topic, group, type(value))
			elif type(value) != group.inferred_type:
				raise ValueError(f'Tried to publish value of type "{type(value)}" on a topic with an inferred type of "{group.inferred_type}"')

			for subscription in group.callbacks:
				subscription(value)

