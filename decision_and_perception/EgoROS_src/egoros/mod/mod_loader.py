from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass
import logging
from types import ModuleType 
import os.path

import importlib.util
import inspect

import sys
import os.path as pth
from typing import List

log = logging.getLogger('egoros')

@dataclass
class ModuleInformation:
	module: ModuleType
	important_files: List[str]

def __check_node_requirements(mod: ModuleType):
	# Check if it contains the neccessary functions
	if not hasattr(mod, 'init'):
		raise ModuleNotFoundError('Module lacks the init(handler) function')

	def check_arguments(func: Callable, maxargs: int):
		spec = inspect.getfullargspec(func)
		# Check argument
		if len(spec.args) > maxargs or len(spec.args) <= 0:
			raise ModuleNotFoundError(f'The module {func} function should only contain at most {maxargs} {"argument" if maxargs == 1 else "arguments"}')

	check_arguments(mod.init, 2)
	# Check if contains a tick function
	if hasattr(mod, 'tick'):
		check_arguments(mod.tick, 1)


def __load_normal(path: str, modname: str = 'file_node', check = True) -> ModuleInformation:
	spec = importlib.util.spec_from_file_location(modname, path)

	if spec == None:
		raise ImportError('Could not generate spec from path')
	if spec.loader == None:
		raise ImportError('Spec loader not present')

	mod = importlib.util.module_from_spec(spec)

# Initialize the module
	spec.loader.exec_module(mod)
	if check:
		__check_node_requirements(mod)

	return ModuleInformation(
			module=mod,
			important_files=[path]
			)

def load_package(path: str):
	return __load_package(path, check=False)

def __load_package(path: str, check = True) -> ModuleInformation:
	# Check if folder contains __init__.py file
	init_path = os.path.join(path, '__init__.py')
	if not os.path.isfile(init_path):
		raise ModuleNotFoundError('Specified folder path does not contain a __init__.py file, hence is not a valid Python package')

	modname = os.path.split(path)[-1]

	# Add parent folder to path
	parent = os.path.join(path, os.path.pardir)
	if not parent in sys.path:
		sys.path.append(parent)

	# Add all files from folder to important list
	def recursive_scan(path: str) -> List[str]:
		pyfiles: List[str] = []
		for node in os.scandir(path):
			if node.is_file() and pth.splitext(node.path)[-1] == '.py':
				pyfiles.append(node.path)
			elif node.is_dir():
				pyfiles.extend(recursive_scan(node.path))
		return pyfiles

	important_files = recursive_scan(path)

	return ModuleInformation(
		module=__load_normal(init_path, modname=modname, check=check).module,
		important_files=important_files
	)

def __load_dynamic(path: str) -> ModuleInformation:
	pass

def load_node(path: str) -> ModuleInformation:
	"""
	Loads a node from a specified path

	Args:
		path (str): module path

	Returns:
		ModuleType: loaded module

	Raises:
		FileNotFoundError: if the module does not exit
		ModuleNotFoundError: if the module is not valid
		ImportError: if the module could not be imported for some other reason
	"""
	# Infer module type
	if path.endswith('.py'):
		return __load_normal(path)
	elif path.endswith('.so'):
		return __load_dynamic(path)
	elif os.path.isdir(path):
		return __load_package(path)
	else:
		raise ModuleNotFoundError(f'Could not determine node type from path: {path}')


def name_from_path(path: str) -> str:
	filename = pth.split(path)[-1]
	splitted = pth.splitext(filename)	
	return splitted[0]
