from __future__ import annotations
from types import ModuleType
from typing import Any, Callable, Dict, List, Optional, Type
import typing

from .topic.topic import Message, TopicCallback_t
from .mod_loader import name_from_path
from .extension_interface import Extension, ExtensionInitMethod_t, ExtensionInterface, NodeName_t, OnPublishCallback_t, OnSubscribeCallback_t, OnNodeStateChangeCallback_t, OnOrchestratorStateChangeCallback_t, TopicName_t
from .mod_loader import load_package

import os
import os.path as pth
import inspect

import logging
log = logging.getLogger('egoros')

def __check_nargs(func: Callable, nargs: int | List[Type]) -> bool:
	spec = inspect.getfullargspec(func)
	if isinstance(nargs, list):
		if len(spec.args) != len(nargs):
			return False

		# TODO: make this implementation work, I need some utils functionality
		# for t, argname in zip(nargs, spec.args):
		# 	if argname in spec.annotations:
		# 		if t != None and t != spec.annotations[argname]:
		# 			return False
		return True
	elif isinstance(nargs, int):
		return len(spec.args) == nargs

def __get_func_from_package(name: str, package: ModuleType, requirements: int | List[Type]) -> Optional[Callable]:
	if hasattr(package, name):
		func = getattr(package, name)
		return func if __check_nargs(func, requirements) else None

	return None

def __get_interface_from_package(path: str) -> ExtensionInterface:
	package = load_package(path).module

	on_publish: Optional[OnPublishCallback_t] = None
	on_subscribe: Optional[OnSubscribeCallback_t] = None
	on_node_state_change: Optional[OnNodeStateChangeCallback_t] = None
	on_orchestrator_state_change: Optional[OnNodeStateChangeCallback_t] = None

	if hasattr(package, 'init'):
		init_extension: ExtensionInitMethod_t = package.init
		__check_nargs(init_extension, [Dict[str, Any]])
	else:
		raise ImportError(f'Failed to get init method from {package}')

	on_publish = __get_func_from_package('on_publish', package, [TopicCallback_t, None])
	on_subscribe = __get_func_from_package('on_subscribe', package, [NodeName_t, TopicName_t])
	on_node_state_change = __get_func_from_package('on_node_state_change', package, [NodeName_t, None])
	on_orchestrator_state_change = __get_func_from_package('on_orchestrator_state_change', package, [None])

	return ExtensionInterface(
		on_publish=on_publish,
		on_subscribe=on_subscribe,
		on_node_state_change=on_node_state_change,
		on_orchestrator_state_change=on_orchestrator_state_change,
		init_extension=init_extension
	)

def load_extensions_from_path(path: str) -> Dict[str, Extension]:
	"""
	Scans a folder for extensions and loads the found packages as extension
	"""
	located_packages: List[str] = []
	for node in os.scandir(path):
		if node.is_dir():
			# Check if it's a python package
			init_file = pth.join(node.path, '__init__.py')
			if pth.exists(init_file):
				located_packages.append(node.path)

	log.info(f'''
Found {len(located_packages)} extensions in {path}:
	{located_packages}
	''')
	# Try to load packages as extensions
	named_paths = ((path, name_from_path(path)) for path in located_packages)
	
	extensions: Dict[str, Extension] = {}
	for path, name in named_paths:
		extension = Extension(
			name=name,
			interface=__get_interface_from_package(path)
		)
		extensions[name] = extension

	return extensions
