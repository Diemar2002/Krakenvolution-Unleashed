from __future__ import annotations
from collections import namedtuple
from types import ModuleType
from typing import Any, Dict, Optional, Type, Union, Callable

from watchdog.observers.api import BaseObserver

from .topic.topic import Message
from .mod_loader import load_node, name_from_path
from .topic.topic_handler import TopicHandler
from .node_handler import NodeHandler, NodeState
from .subscription_store import SubscriptionStore

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from datetime import datetime, timedelta
import threading as th
from dataclasses import dataclass
from queue import Empty, Queue
from enum import Enum
import inspect

import logging
log = logging.getLogger('egoros')

@dataclass
class Configuration:
    tick_rate: float = 10

QueuedMessage = namedtuple('QueuedMessage', ['topic', 'msg'])

class Node(FileSystemEventHandler):
	"""
	Wrapper class for the ModuleType class. Contains functionality to manage a
		single node (python, C++, etc)
	
	Attributes:
		module_path (str): path where the base module is located
	Methods:
		enable_hot_reloading(): enbles hot reloading in this node
		is_hot_reloading_enabled(): checks if the hot reloading feature is active
		start(): starts all subprocesses
		stop(): stops all subprocesses
		publish(str, Message): sends a value to all internal registered subscriptions
	"""

	def __init__(self, path: str, topic_handler: TopicHandler, node_handler: NodeHandler, Queue_t: Type = Queue) -> None:
		"""
		Creates a new Node
		
		Args:
			path (str): path where the store will be selected
			topic_handler (TopicHandler): contains callback methods for publish and subscribe 
			node_handler (NodeHandler): contains callback methods for node related stuff
			Queue_t (Type): type to instantiate for the message queue
				The recommended value depends on how will the node be executed
				- If the node is running as a thread a normal queue should be used,
				  as it's thread-safe (thanks GIL)
				- If the noed is running as another process with the
				  multiprocessing library, a multiprocessing.Queue should be used
				  as the data wouldn't be shared otherwise
		"""

		self.__running = False

		self.__module_path = path
		self.__name = name_from_path(path)
		self.__node: Optional[ModuleType] = None
		self.__node_handler = node_handler
		self.__observer: Optional[BaseObserver] = None
		self.__orchestrator_topic_handler = topic_handler
		self.__subscription_store = SubscriptionStore()

		self.status = NodeState.UNLOADED
		self.__set_state(NodeState.UNLOADED)

		# Set reloading variables
		self.__last_reload_time = datetime.min
		# self.__reload_timeout = timedelta(seconds=1)
		self.__reload_timeout = timedelta(seconds=0)
		self.__reload()
		self.__init_node_function: Optional[Callable[[], Optional[Any]]] = None

		# Run variables
		self.__message_queue: Queue[QueuedMessage] = Queue_t()
		self.__tick_thread_handler: Optional[th.Thread] = None
		self.__topic_thread_handler: Optional[th.Thread] = None
		self.__stop_event = th.Event()
		self.__new_message_event = th.Event()

	@property
	def name(self):
		return self.__name

	@property
	def module_path(self):
		return self.__module_path

	def __set_state(self, new_state: NodeState):
		log.debug(f'Node changed state from {self.status} to {new_state}')
		self.status = new_state
		self.__node_handler.on_node_state_changed(new_state)

	def __reload(self):
		log.debug('Reloading')
		self.__subscription_store.reset()
		self.__set_state(NodeState.LOADING)
		try:
			self.__node = load_node(self.module_path)
			self.__set_state(NodeState.LOADED)
			if self.__running and self.__init_node_function is not None:
				self.__init_node_function()
		except Exception as e: 
			self.__set_state(NodeState.FAILED_TO_LOAD)
			log.error(f'''
	Node {self} crashed during loading
		{e}
								 ''')
		return 


	def on_modified(self, event):
		"""
		This funcion comes inherited from the FileSystemEventHandler superclass.
		This method will be executed every time the file associated with the
		node has updated.
		It has debouncing logic built into it.

		Args:
			event: file system update event
		"""
		if not event.is_directory and event.src_path == self.module_path:
			log.info(f'Change detected in "{self.module_path}" Reloading node...')

			now = datetime.now()
			if (now - self.__last_reload_time) < self.__reload_timeout:
				log.info(f'Node reload event for "{self.module_path}" triggered in less than {self.__reload_timeout}')
			else:
				self.__reload()
				self.__last_reload_time = now

	def enable_hot_reloading(self, timeout: Optional[timedelta] = None):
		"""
		Enables hot reload for this node
		"""
		# Set timeout
		if timeout is not None:
			self.__reload_timeout = timeout
		# TODO: add filesystem observer
		if self.__observer == None:
			self.__observer = Observer()

		self.__observer.schedule(self, path=self.module_path, recursive=True)
		self.__observer.start()

	def is_hot_reloading_ready(self) -> bool:
		"""
		Checks if the thread that checks if there is a change has been started yet

		Returns:
			True if the thread has started, False if the thread has not been
			started or hot reloading has not been enabled on thise node.
		"""

		return (self.__observer != None) and (self.__observer.is_alive())

	def start(self, config: Dict[str, Any] = {}) -> None:
		"""
		Starts all subprocesses related to the node as threads.
		Specifically starts a thread for reading the message queue and another
		one for updating the tick function.
		This function returns immediately. These threads can only be stopped
		with the internal stop method.

		The node should be completely loaded before calling this function and it
		should not be called if already running.

		Raises:
			SystemError: if the node is already running
			SystemError if the node has not been loaded (maybe crashed during
				initialization).
		"""

		if self.__running:
			raise SystemError('The node is already running')
		if self.status != NodeState.LOADED:
			log.error(f'Node is {self.status}, should be {NodeState.LOADED} to be able to be run')
			return

		def topic_handler_on_subscribe(*args):
			self.__subscription_store.subscribe(*args)
			self.__orchestrator_topic_handler.subscribe(*args)
		# Generate neccessary handlers
		topic_handler = TopicHandler(
			publish=self.__orchestrator_topic_handler.publish,
			subscribe=topic_handler_on_subscribe
		)
		# Run workers
		def init_node() -> Optional[Any]:
			try:
				# Check if has argument for configuration
				init = self.__node.init
				spec = inspect.getfullargspec(init)
				if len(spec.args) == 1:
					node_cfg = init(topic_handler)
				elif len(spec.args) == 2:
					node_cfg = init(topic_handler, config)
				else:
					raise ValueError(f'The init function contains more than 2 arguments')

				return node_cfg
			except Exception as e:
				log.error(f'Failed to initialize node {self.__node} with exception\n{e}')
				self.__set_state(NodeState.FAILED_TO_INIT)
				return None

		node_cfg = init_node()
		self.__init_node_function = init_node

		self.__running = True

		if hasattr(self.__node, 'tick'):
			self.__tick_thread_handler = th.Thread(target=self.__tick_worker, args=[node_cfg, topic_handler])
			self.__tick_thread_handler.daemon = True # Just in case
			self.__tick_thread_handler.start()

		self.__topic_thread_handler = th.Thread(target=self.__reader_worker, args=[node_cfg])
		self.__topic_thread_handler.daemon = True # Just in case
		self.__topic_thread_handler.start()

	def __tick_worker(self, node_cfg: Configuration, handler: TopicHandler) -> None:
		from datetime import datetime, timedelta
		from time import sleep
		tick_delta = timedelta(seconds=1.0 / node_cfg.tick_rate)
		next_tick =  datetime.now() + tick_delta
		try:
			while self.__running:
				self.__node.tick(handler)
				now = datetime.now()
				if now < next_tick:
					self.__stop_event.wait((next_tick - now).total_seconds())
					next_tick += tick_delta
				else:
					pass # TODO: warn the user that the node is running slower than the specified tick rate
		except Exception as e:
			self.__set_state(NodeState.CRASHED)
			log.error(f'Node {self.__node} crashed during tick operation with exception:\n{e}')

	def __reader_worker(self, node_cfg: Configuration) -> None:
		while self.__running:
			try: 
				msg = self.__message_queue.get(block=False)
				self.__subscription_store.publish(msg.topic, msg.msg)
			except Empty:
				self.__new_message_event.wait()
				self.__new_message_event.clear()

	def stop(self, timeout: float | None = None) -> None:
		"""
		Stops all running processes for this node. 

		Should only be called if the node is already running

		KeywordArgs:
			timeout (float | None): maximum time that the join can last. 

		Raises:
			RuntimeError: if the specified timeout for the joining has been
			reached.
		"""

		if self.__running:
			self.__running = False
			self.__stop_event.set()
			if self.__tick_thread_handler is not None:
				self.__tick_thread_handler.join()
			if self.__topic_thread_handler is not None:
				self.__new_message_event.set() # Exit is a message but it won't try to read
				self.__topic_thread_handler.join()
		else:
			raise RuntimeError('Can not stop non-running node')

	# Publish
	def publish(self, topic: str, value: Message):
		"""
		Sends a message to all registered subscriptions for this node.

		Args:
			topic (str): name of the topic to publish the message
			value (Message): message to be sent to the node
		"""

		self.__message_queue.put(QueuedMessage(topic, value))
		self.__new_message_event.set()

	# Primary testing functions 
	def tick(self):
		self.__node.tick(self.__node_handler)
