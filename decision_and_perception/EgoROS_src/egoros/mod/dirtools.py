from __future__ import annotations

import os
import os.path
from typing import List

VALID_NODE_EXTENSIONS = ['.py', '.so']

def scan_folder_for_nodes(path: str) -> List[str]:
	def __path_walk(path: str) -> List[str]:
		scanned: List[str] = []
		for file in os.scandir(path):
			if file.is_file():
			# Check if it has a valid extension
				splitted = os.path.splitext(file.path)
				if len(splitted) == 2 and splitted[-1] in VALID_NODE_EXTENSIONS:
					# Add to list
					scanned.append(os.path.abspath(file.path))
			elif file.is_dir():
				# Check if it's a valid package (contains __init__.py file)
				init_dir = os.path.join(file.path, '__init__.py')
				if os.path.exists(init_dir):
						scanned.append(file.path)
				else:
					# If it's not a package, read it as a directory
					scanned.extend(__path_walk(file.path))
		return scanned

	return __path_walk(path)

