import logging
from rich.logging import RichHandler
from rich.traceback import install
from logging.handlers import TimedRotatingFileHandler

def configure_logger(package_name: str, log_file: str):
	"""
	Configures the logger

	Args: 
		package_name (str): name of the package that runs the logging. It's the
			value that will have to be passed to the logging.get_logger function.
		log_file (str): filename where the logs will be stored into.
		TODO: add feature to leave the file to None (the logs will not be stored)
	"""
	install()  # Install the rich handler for better exception logging

	# Create a logger object
	logger = logging.getLogger(package_name)
	logger.setLevel(logging.INFO)

	# Create a console handler with level INFO
	console_handler = RichHandler()
	console_handler.setLevel(logging.INFO)
	# We want to limit the console handler to only the log messages, without timestamp
	console_format = logging.Formatter("%(message)s")
	console_handler.setFormatter(console_format)

	# Create a file handler which logs INFO messages
	file_handler = TimedRotatingFileHandler(log_file, when='midnight')
	file_handler.setLevel(logging.INFO)
	# For the file handler, we want a more elaborate format, including the timestamp
	file_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	file_handler.setFormatter(file_format)

	# Add the handlers to the logger
	logger.addHandler(console_handler)
	logger.addHandler(file_handler)

	return logger
