from __future__ import annotations
from enum import Enum
from typing import Any, Dict, List

from .extension_interface import Extension, ExtensionStore
from .node import Node
from .node_handler import NodeHandler, NodeState

from .topic.topic import Message, TopicCallback_t

from .topic.topic_handler import TopicHandler

import logging
log = logging.getLogger('egoros')

class OrchestratorStatus(Enum):
	STOPPED = 0
	RUNNING = 1
	NODES_CRASHED = 2

class Orchestrator:
	"""
	Class that manages a group of nodes. Acts as a bridge between the nodes. 
	Also helps with tapping into the communications for other tools to use those values

	Methods:
		hook_handler(topicHandler): adds a topic handler whose methods will be
			called based on the messages sent by the managed nodes
		publish(str, Any): Sends a message to the managed nodes. It's also used
			by the nodes
		start(): initializes all of the nodes and threads to run them on
		stop(): stops all of the threads
	"""

	def __init__(self, nodes: List[str]) -> None:
		"""
		Initializes the orchestrator creating all of the nodes (no node methods
		are called from here)

		Args:
			nodes (List[str]): list of paths where the nodes that will be opened
				are located
		"""
		self.__running: bool = False

		# Optional callbacks
		self.__hooked_handlers: List[TopicHandler] = []
		self.__hooked_extensions: List[Extension] = []
		self.__extension_store = ExtensionStore()

		self.__topic_handler = TopicHandler(
			publish=self.publish,
			subscribe=self.__subscribe
		)

		# Initialize nodes
		self.__nodes: List[Node] = []

		self.__status: OrchestratorStatus = OrchestratorStatus.STOPPED
		self.__set_status(OrchestratorStatus.STOPPED)

		self.__nodes = [Node(
			path=path,
			topic_handler = self.__topic_handler,
			node_handler=NodeHandler(
				on_node_state_changed=
					lambda status, path=path: self.__node_state_changed(path, status)
			)
		) for path in nodes]


	def __pull_node_status(self):
		# Check states of all nodes and update status acordingly
		any_crashed = any([node.status in (
			NodeState.CRASHED,
			NodeState.FAILED_TO_LOAD,
			NodeState.FAILED_TO_INIT
		) for node in self.__nodes])

		if self.__running:
			if any_crashed:
				self.__set_status(OrchestratorStatus.NODES_CRASHED)
			else:
				self.__set_status(OrchestratorStatus.RUNNING)
		else:
			self.__set_status(OrchestratorStatus.STOPPED)

	def __node_state_changed(self, node: str, status: NodeState):
		log.debug(f'Node {node} changed status')
		self.__extension_store.run_on_node_state_change(node, status)
		self.__pull_node_status()

	def hook_handler(self, handler: TopicHandler):
		"""
		Adds callbacks to publish and subscribe events on the message bus

		Args:
			handler (TopicHandler): topic handler that contains a publish and
				subscribe callback
		"""

		self.__hooked_handlers.append(handler)

	def hook_extension(self, extension: Extension) -> None:
		"""

		"""
		self.__hooked_extensions.append(extension)
		self.__extension_store.hook_interface(extension.interface)


	def publish(self, topic: str, value: Any):
		"""
		Sends a message to the nodes

		Args:
			topic (str): topic to send the message to
			value (Any): message to publish
		"""
		for node in self.__nodes:
			node.publish(topic, value)
		
		# Update hooks
		for hook in self.__hooked_handlers:
			hook.publish(topic, value)
		self.__extension_store.run_on_publish(topic, value)

	def __subscribe(self, topic: str, callback: TopicCallback_t):
		for hook in self.__hooked_handlers:
			hook.subscribe(topic, callback)
		# TODO: add node name
		self.__extension_store.run_on_subscribe('node', topic)

	def start(self, config: Dict[str, Any] = {}):
		"""
		Initializes all of the registered nodes
		"""

		# Start all nodes
		self.__running = True
		self.__pull_node_status()
		[node.start(config.get(node.name, {})) for node in self.__nodes]

	def stop(self):
		"""
		Stops all of the registered nodes
		"""
		log.info('Stopping orchestrator')
		self.__running = False
		self.__pull_node_status()
		for node in self.__nodes:
			if node.status == NodeState.LOADED:
				try:
					node.stop()
				except RuntimeError as e:
					log.warning(f'Could not stop node {node.module_path} because it was not started. Ignoring')


	def enable_hot_reloading(self):
		"""
		Enables hot reloading for all of the registered nodes
		"""
		for node in self.__nodes:
			node.enable_hot_reloading()

	def __set_status(self, status: OrchestratorStatus):
		log.debug(f'Orchestrator changed state from {self.__status} to {status}')
		self.__status = status
		self.__extension_store.run_on_orchestrator_state_change(self.__status)

	def status(self):
		"""
		Returns the current status of the orchestrator. They mean the following:
			- STOPPED: the orchestrator hasn't been started yet.
			- RUNNING: all the nodes are running without issues.
			- NODES_CRASHED: some nodes have crashed.
		"""
		return self.__status
