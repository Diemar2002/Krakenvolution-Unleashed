from __future__ import annotations
from collections.abc import Callable

from dataclasses import dataclass, field
from typing import Any, Generic, TypeVar

from datetime import datetime


@dataclass
class MessageContext:
    time: datetime = field(default_factory=datetime.now)

Message_t = TypeVar('Message_t')

@dataclass
class Message(Generic[Message_t]):
    value: Message_t
    ctx: MessageContext
    
TopicCallback_t = Callable[[Any], Any]
