from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Generic, TypeVar, Callable

from ..topic.topic import TopicCallback_t

@dataclass
class TopicHandler():
    publish: Callable[[str, Any], None]
    subscribe: Callable[[str, TopicCallback_t], None]



