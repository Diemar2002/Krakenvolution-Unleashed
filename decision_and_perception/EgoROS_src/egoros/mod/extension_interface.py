from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Dict, List, Optional

from .topic.topic import Message

NodeName_t = str
TopicName_t = str

OnPublishCallback_t = Callable[[TopicName_t, Message[Any]], None]
OnSubscribeCallback_t = Callable[[NodeName_t, TopicName_t], None]
OnOrchestratorStateChangeCallback_t = Callable[[Any], None]
OnNodeStateChangeCallback_t = Callable[[NodeName_t, Any], None]
ExtensionInitMethod_t = Callable[[Callable[[str, Any], None], Dict[str, Any]], None]

@dataclass
class ExtensionInterface:
	on_publish: Optional[OnPublishCallback_t]
	on_subscribe: Optional[OnSubscribeCallback_t]
	on_orchestrator_state_change: Optional[OnOrchestratorStateChangeCallback_t]
	on_node_state_change: Optional[OnNodeStateChangeCallback_t]
	init_extension: ExtensionInitMethod_t

class ExtensionStore:
	def __init__(self) -> None:
		self.__callbacks = {
				'on_publish': [],
				'on_subscribe': [],
				'on_orchestrator_state_change': [],
				'on_node_state_change': []
				}

	def hook_interface(self, interface: ExtensionInterface) -> None:
		for attr_name, callback_list in self.__callbacks.items():
			attr_value = getattr(interface, attr_name)
			if attr_value is not None:
				callback_list.append(attr_value)

	def run_on_publish(self, topic_name: str, message: Message[Any]) -> None:
		for callback in self.__callbacks['on_publish']:
			callback(topic_name, message)

	def run_on_subscribe(self, node_name: NodeName_t, topic_name: TopicName_t) -> None:
		for callback in self.__callbacks['on_subscribe']:
			callback(node_name, topic_name)

	def run_on_orchestrator_state_change(self, state: Any) -> None:
		for callback in self.__callbacks['on_orchestrator_state_change']:
			callback(state)

	def run_on_node_state_change(self, node_name: NodeName_t, state: Any) -> None:
		for callback in self.__callbacks['on_node_state_change']:
			callback(node_name, state)

@dataclass
class Extension:
    name: str
    interface: ExtensionInterface
