from __future__ import annotations
from dataclasses import dataclass, field
from datetime import timedelta
from typing import Any, Callable, Dict, List, Optional

from .topic.topic_handler import TopicHandler
from .node_handler import NodeHandler, NodeState
from .topic.topic import Message
from .mod_loader import ModuleInformation, load_node, name_from_path
from .subscription_store import SubscriptionStore

from watchdog.events import FileModifiedEvent, FileSystemEventHandler
from watchdog.observers import Observer

import inspect
import traceback

import os.path as pth
import threading as th
from queue import Queue, Empty

@dataclass
class Configuration:
	tick_rate: float = 10.0

@dataclass
class QueuedMessage:
	topic: str
	message: Any

import logging
log = logging.getLogger('egoros')

class ImportantFileChangeHandler(FileSystemEventHandler):
	def __init__(self, on_update: Callable[[], None], important_files: List[str]) -> None:
		self.__on_update = on_update
		self.__important_files = important_files

	def on_modified(self, event: FileModifiedEvent):
		try:
			if isinstance(event, FileModifiedEvent) and any(pth.samefile(event.src_path, file) for file in self.__important_files):
				log.info(f'Detected change in {event.src_path}')
				self.__on_update()
		except FileNotFoundError as e:
			log.warning(f'Failed to interpret inotify event for {event.src_path}')

class Node:
	def __init__(self, path: str, topic_handler: TopicHandler, node_handler: NodeHandler) -> None:
		self.__path = path
		self.__node_state: NodeState = NodeState.UNLOADED
		self.__node_handler = node_handler
		self.__topic_handler = topic_handler

		self.__name = name_from_path(path)
		self.__node: ModuleInformation

		# State
		self.__running: bool = False
		self.__subscription_store: SubscriptionStore
		self.__stop_event = th.Event()

		# Threads
		self.__reader_thread: Optional[th.Thread] = None
		self.__tick_thread: Optional[th.Thread] = None

		self.__load_internal()

		# Message sending stuff
		self.__new_message_event = th.Event()
		self.__message_queue: Queue[QueuedMessage] = Queue()
		self.__topic_handler_for_inner_node: Optional[TopicHandler] = None
		self.__prev_loaded_configuration: Optional[Dict[str, Any]] = None

		def on_hot_reload():
			try:
				log.info(f'"{self.__name}": reloading...')

				if hasattr(self.__node.module, 'on_reload'):
					log.info('Does have on_reload')
					self.__node.module.on_reload()

				self.__load_internal()
				self.__init_internal(self.__prev_loaded_configuration if self.__prev_loaded_configuration is not None else {})
			except Exception as e:
				log.error(f'Failed when reloading: {e}')
				raise e

		# Hot reloading stuff
		self.__hot_reloading_server = Observer()
		self.__hot_reload_handler = ImportantFileChangeHandler (
			important_files=self.__node.important_files,
			on_update=on_hot_reload
		)
		self.__hot_reloading_server.schedule(
			event_handler=self.__hot_reload_handler,
			path=path
		)

	@property
	def name(self) -> str:
		return self.__name

	@property
	def module_path(self) -> str:
		return self.__path

	@property
	def status(self) -> NodeState:
		return self.__node_state

	def __change_state(self, state: NodeState):
		prev_node = self.__node_state
		self.__node_state = state
		log.debug(f'"{self.__name}" {prev_node} -> {self.__node_state}') 
		self.__node_handler.on_node_state_changed(self.__node_state)

	def __load_internal(self):
		self.__node_state = NodeState.LOADING
		try:
			self.__node = load_node(self.__path)
			self.__subscription_store = SubscriptionStore()
		except FileNotFoundError as e:
			log.warning(f'Failed to open a file while loading the node "{self.__name}":\n{e}')
			self.__change_state(NodeState.FAILED_TO_LOAD)
		except ModuleNotFoundError or ImportError as e:
			log.warning(f'Failed to load node "{self.__name}":\n{e}')
			self.__change_state(NodeState.FAILED_TO_LOAD)

	def __init_internal(self, config: Dict[str, Any]) -> Optional[Configuration]:
		try:
			init = self.__node.module.init
			spec = inspect.getfullargspec(init)
			if len(spec.args) == 1:
				node_cfg = init(self.__topic_handler_for_inner_node)
			elif len(spec.args) == 2:
				node_cfg = init(self.__topic_handler_for_inner_node, config)
			else:
				raise ValueError(f'The init function contains more than 2 arguments')

			# Check if returned configuration (not this problem again for fuck's sake)
			if not isinstance(node_cfg, Configuration):
				raise AssertionError(f'init function should return an instance of {Configuration}')

			return node_cfg
		except Exception as e:
			log.warning(f'Failed to initialize node "{self.__name}": {e}')
			traceback_str = ''.join(traceback.format_exception(type(e), e, e.__traceback__))
			log.warning(traceback_str)
			self.__change_state(NodeState.FAILED_TO_INIT)


		return None

	# Workers
	def __reader_worker(self) -> None:
		while self.__running:
			try:
				msg = self.__message_queue.get(block=False)
				self.__subscription_store.publish(msg.topic, msg.message)
			except Empty:
				self.__new_message_event.wait()
				self.__new_message_event.clear()

	def __tick_worker(self, node_cfg: Configuration, node_handler: NodeHandler) -> None:
		from datetime import datetime, timedelta
		tick_delta = timedelta(seconds=1.0 / node_cfg.tick_rate)
		next_tick =  datetime.now() + tick_delta
		try:
			while self.__running:
				self.__node.module.tick(node_handler)
				now = datetime.now()
				if now < next_tick:
					self.__stop_event.wait((next_tick - now).total_seconds())
					self.__stop_event.clear()
					next_tick += tick_delta
				else:
					pass # TODO: warn the user that the node is running slower than the specified tick rate
		except Exception as e:
			self.__change_state(NodeState.CRASHED)
			log.error(f'Node {self.__node} crashed during tick operation with exception:\n{e}')

	def enable_hot_reloading(self, timeout: Optional[timedelta] = None):
		self.__hot_reloading_server.start()

	def start(self, config: Dict[str, Any] = {}) -> None:
		self.__prev_loaded_configuration = config
		# Create reader thread
		if self.__running:
			raise SystemError(f'The node {self.__name} is already running')

		def topic_handler_on_subscribe(*args):
			log.info(f'Subscription with args: {args}')
			self.__subscription_store.subscribe(*args)
			self.__topic_handler.subscribe(*args)

		topic_handler = TopicHandler(
			publish=self.__topic_handler.publish,
			subscribe=topic_handler_on_subscribe
		)

		self.__topic_handler_for_inner_node = topic_handler

		node_cfg = self.__init_internal(config)

		if node_cfg is not None:
			self.__running = True
			if hasattr(self.__node.module, 'tick'):
				self.__tick_thread = th.Thread(target=self.__tick_worker, args=[node_cfg, topic_handler])
				self.__tick_thread.daemon = True
				self.__tick_thread.start()

			self.__reader_thread = th.Thread(
				target=self.__reader_worker
			)
			self.__reader_thread.daemon = True
			self.__reader_thread.start()
			

	def stop(self) -> None:
		if self.__running:
			# self.__change_state(NodeState.LOADED)
			if self.__hot_reloading_server.is_alive():
				self.__hot_reloading_server.stop()

			if self.__reader_thread is not None and self.__reader_thread.is_alive():
				self.__new_message_event.set()
			if self.__tick_thread is not None and self.__tick_thread.is_alive():
				self.__stop_event.set()

			self.__running = False
		else:
			raise RuntimeError(f'Can\'t stop non running node {self.__name}')

	def publish(self, topic: str, value: Message):
		self.__message_queue.put(QueuedMessage(
			topic=topic,
			message=value
		))

		self.__new_message_event.set()
