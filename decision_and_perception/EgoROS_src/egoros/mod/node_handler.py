from __future__ import annotations

from dataclasses import dataclass
from typing import Callable
from enum import Enum

class NodeState(Enum):
	LOADED = 0
	CRASHED = 1
	FAILED_TO_INIT = 2
	FAILED_TO_LOAD = 3
	LOADING = 4
	UNLOADED = 5

@dataclass
class NodeHandler:
    """
    A class that handles node state changes.

    Attributes:
		  on_node_state_changed (Callable[[NodeState], None]): A callback
			  function that is called when the state of a node changes.
			  The state will be one of the NodeState enum
    """

    on_node_state_changed: Callable[[NodeState], None]
