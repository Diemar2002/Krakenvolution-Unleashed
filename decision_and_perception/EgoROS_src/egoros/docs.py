from collections.abc import Callable
import os.path
from typing import Dict, Optional
import rich.tree as tree
from rich.markdown import Markdown
from rich import print

CURRENT_FILE = os.path.dirname(__file__)
DOCS_FOLDER = os.path.join(CURRENT_FILE, 'docs')

DocsRoot_t = Dict[str, Optional['DocsRoot_t']]
# Read docs file structure
docs_tree: Optional[DocsRoot_t] = None
def __check_if_loaded(func: Callable) -> Callable:
    def __func(*args):
        global docs_tree
        # Check if docs have been loaded
        if docs_tree == None:
            def __walk_directory(path: str) -> DocsRoot_t:
                entries: DocsRoot_t = {}
                for entry in os.scandir(path):
                    # Check if file or directory
                    if entry.is_file() and entry.path.endswith('.md'):
                        entries[os.path.splitext(entry.name)[0]] = None
                    elif entry.is_dir():
                        entries[entry.name] = __walk_directory(entry.path)

                return entries

            docs_tree = __walk_directory(DOCS_FOLDER)

        # Call underlying function
        func(*args)

    return __func


@__check_if_loaded
def print_tree():
    # Convert tree into dictionary
    def __recursion(d: DocsRoot_t, branch: tree.Tree):
        for filename, entry in d.items():
            if entry == None:
                branch.add(f' {filename}')
            else:
                new_branch = tree.Tree(f' {filename}')
                __recursion(entry, new_branch)
                branch.add(new_branch)

    root = tree.Tree(' Docs')
    __recursion(docs_tree, root)
    print(root)

@__check_if_loaded
def get_docs_as_dict():
    return docs_tree

@__check_if_loaded
def print_docs(path: str):
    # Try to open
    with open(os.path.join(DOCS_FOLDER, path) + '.md', 'r') as doc:
        markdown = Markdown(doc.read())
        print(markdown)
            
