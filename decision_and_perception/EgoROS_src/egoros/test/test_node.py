from queue import Queue
import pytest
from unittest.mock import Mock
from ..mod.node_handler import NodeHandler, NodeState
from typing import Type

from ..mod.topic.topic_handler import TopicHandler
from ..mod.node import Node
from .utils import gen_file_structure, WaitableMock, DEFAULT_TICKLESS_NODE

from time import sleep
import threading as th

class ManagedNode(Node):
	def __init__(self, path: str, topic_handler: TopicHandler, node_handler: NodeHandler)-> None:
		super().__init__(path, topic_handler, node_handler)

	def __enter__(self):
		self.start()

	def __exit__(self, type, value, traceback):
		self.stop()

def gen_publisher(rate: float):
	pub_file = gen_file_structure(f'''
import egoros

def init(handler, config):
	 return egoros.Configuration(
		  tick_rate={rate}
	 )

def tick(handler):
	 handler.publish('topic', 123)
											 ''', suffix='.py')
	return pub_file

def gen_mocked_handlers():
	pub_mock = WaitableMock() 
	sub_mock = WaitableMock() 

	handler = TopicHandler(
		publish=pub_mock,
		subscribe=sub_mock
	)

	on_changed = WaitableMock()
	node_handler = NodeHandler(
		on_node_state_changed=on_changed
	)

	return pub_mock, sub_mock, handler, node_handler

def test_stopping():
	file = gen_file_structure(DEFAULT_TICKLESS_NODE, suffix='.py')
	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	node = ManagedNode(
		path=file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	with node:
		pass

def test_stopping_exceptions():
	file = gen_file_structure(DEFAULT_TICKLESS_NODE, suffix='.py')
	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	node = ManagedNode(
		path=file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	with pytest.raises(RuntimeError):
		node.stop()

	with node:
		pass

	with pytest.raises(RuntimeError):
		node.stop()

def test_publish_node():
	pub_file = gen_publisher(1)

	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	pub_node = ManagedNode(
		path=pub_file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	with pub_node:
		pub_mock.event.wait()

	# Check if the mocks have been called
	pub_mock.assert_called_with('topic', 123)
	assert pub_mock.call_count == 1

def test_tick_rate():
	pub_file = gen_publisher(10)

	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	pub_node = ManagedNode(
		path=pub_file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	with pub_node:
		sleep(0.5)

	assert pub_mock.call_count == pytest.approx(5, rel=2)

def test_subscriptions():
	sub_file = gen_file_structure(f'''
import egoros

def callback(message):
	 message(123)

def init(handler, config):
	 handler.subscribe('topic', callback)
	 return egoros.Configuration()
											 ''', suffix='.py')

	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	sub_node = ManagedNode(
		path=sub_file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	message = WaitableMock()
	with sub_node:
		sub_node.publish('topic', message)
		sub_node.publish('notatopic', message)

		message.event.wait()

	message.assert_called_once_with(123)

def test_init_failiure():
	file = gen_file_structure('''
import egoros

def init(handler):
	raise Exception()
									  ''', suffix='.py')

	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	node = ManagedNode(
		path=file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	pass_event = th.Event()

	def on_node_state_changed(state: NodeState):
		assert state != NodeState.CRASHED
		if state == NodeState.FAILED_TO_INIT:
			pass_event.set()

	node_handler.on_node_state_changed = on_node_state_changed

	# The runtime error occurs when stopping the node as it should
	with pytest.raises(RuntimeError):
		with node:
			pass_event.wait()

	
def test_tick_failiure():
	file = gen_file_structure('''
import egoros

def init(handler, config):
	return egoros.Configuration()

def tick(handler):
	raise Exception()
								     ''', suffix='.py')

	pub_mock, sub_mock, handler, node_handler = gen_mocked_handlers()

	node = ManagedNode(
		path=file.name,
		topic_handler=handler,
		node_handler=node_handler
	)

	pass_event = th.Event()

	def on_node_state_changed(state: NodeState):
		if state == NodeState.CRASHED:
			pass_event.set()

	node_handler.on_node_state_changed = on_node_state_changed

	with node:
		pass_event.wait()
	

