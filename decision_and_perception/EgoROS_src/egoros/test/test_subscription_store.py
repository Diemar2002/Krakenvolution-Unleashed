import pytest
from unittest.mock import Mock, call

from ..mod.subscription_store import SubscriptionStore

def test_normal_behaviour(capfd):
    store = SubscriptionStore()

    mocks = [Mock(), Mock()]

    store.subscribe('test_topic', mocks[0])

    store.publish('test_topic', 123)
    mocks[0].assert_called_once_with(123)

    store.subscribe('test_topic', mocks[1])
    mocks[0].reset_mock()

    store.publish('test_topic', 14)
    mocks[1].assert_called_once_with(14)
    mocks[0].assert_called_once_with(14)

def test_reset():
    store = SubscriptionStore()

    mock = Mock()
    store.subscribe('test_topic', mock)

    store.reset()
    store.publish('test_topic', 123)
    
    mock.assert_not_called()

def test_multiple_subscriptions():
    store = SubscriptionStore()
    mock = Mock()

    store.subscribe('test_topic', mock)
    store.subscribe('another_topic', mock)

    store.publish('test_topic', 123)

    mock.assert_called_once_with(123)
    mock.reset_mock()

    store.publish('another_topic', 456)
    mock.assert_called_once_with(456)

def test_multiple_fuckery():
    store = SubscriptionStore()

    mock = Mock()
    store.subscribe('topic', mock)

    store.publish('not_topic', 123)
    mock.assert_not_called()

# Typing tests
from ..mod.topic.topic import Message 

def test_same_without_hints():
	store = SubscriptionStore()
	mock = Mock()

	store.subscribe('topic', mock)
	store.subscribe('topic', mock)

	store.publish('topic', 123)
	store.publish('topic', 456)
	store.publish('other_topic', 'Kekos')

def test_different_without_hints():
	store = SubscriptionStore()
	mock = Mock()

	store.subscribe('topic', mock)

	store.publish('topic', 123)

	with pytest.raises(ValueError):
		store.publish('topic', 'Kekos')

def test_different_with_hints():
	store = SubscriptionStore()

	def callback(value: Message[int]):
		pass

	store.subscribe('topic', callback)

	with pytest.raises(ValueError):
		store.publish('topic', 'Kekos')
	
def test_multiple_hints():
	store = SubscriptionStore()
	
	def cb1(value: Message[int]):
		pass

	def cb2(value: Message[str]):
		pass

	store.subscribe('topic', cb1)
	
	with pytest.raises(ValueError):
		store.subscribe('topic', cb2)

def test_with_typing_module():
	pytest.skip('TODO: search how to check if two types are compatible')
	from typing import Union, Any
	store = SubscriptionStore()
	mock = Mock()

	def cb1(value: Message[Union[int, str]]):
		pass

	def cb2(value: Message[str]):
		pass

	def cb3(value: Message[Any]):
		pass

	store.subscribe('topic', cb1)
	with pytest.raises(ValueError):
		store.subscribe('topic', cb2)

	store.subscribe('topic', cb3)
