from __future__ import annotations
import pytest
from unittest.mock import Mock, call


from ..mod.extension_interface import Extension, ExtensionInterface, TopicName_t
from ..mod.extensions import load_extensions_from_path
from ..mod.orchestrator import ExtensionStore, Orchestrator
from .test_orchestrator import ManagedOrchestrator
from .utils import WaitableMock, gen_file_structure

def test_extension_store():
	store = ExtensionStore()

	publish_mock = WaitableMock()
	subscribe_mock = WaitableMock()
	orchestrator_state_change_mock = WaitableMock()
	node_state_change_mock = WaitableMock()

	interface= ExtensionInterface(
		on_publish=publish_mock,
		on_subscribe=subscribe_mock,
		on_orchestrator_state_change=orchestrator_state_change_mock,
		on_node_state_change=node_state_change_mock,
		init_extension=lambda _: None
	)

	store.hook_interface(interface)

	store.run_on_publish('topic', 123)
	publish_mock.assert_called_once_with('topic', 123)
	store.run_on_subscribe('node', 'topic')
	subscribe_mock.assert_called_once_with('node', 'topic')
	store.run_on_orchestrator_state_change(123)
	orchestrator_state_change_mock.assert_called_once_with(123)
	store.run_on_node_state_change('node', 123)
	node_state_change_mock.assert_called_once_with('node', 123)
	
	

def test_extensions():
	publisher = gen_file_structure('''
import egoros

def callback(message):
	pass

def init(handler):
	handler.publish('topic', 123)
	handler.subscribe('other_topic', callback)
	return egoros.Configuration()
										  	 ''', suffix='.py')

	nodes = [
		publisher.name
	]
	orchestrator = ManagedOrchestrator(nodes)

	publish_mock = WaitableMock()
	subscribe_mock = WaitableMock()
	orchestrator_state_change_mock = WaitableMock()
	node_state_change_mock = WaitableMock()

	extension = Extension(
		name='subscriber',
		interface= ExtensionInterface(
			on_publish=publish_mock,
			on_subscribe=subscribe_mock,
			on_orchestrator_state_change=orchestrator_state_change_mock,
			on_node_state_change=node_state_change_mock,
			init_extension=lambda _: None
		)
	)

	orchestrator.hook_extension(extension)

	with orchestrator:
		publish_mock.event.wait()
		subscribe_mock.event.wait()
		orchestrator_state_change_mock.event.wait()
		# FIXME: This doesn't work for some reason
		# node_state_change_mock.event.wait()

		# FIXME: For now the default value is node. The name of the node should
		# be passed to the function
		subscribe_mock.assert_called_once_with('node', 'other_topic')
		publish_mock.assert_called_once_with('topic', 123)

def test_extension_loading():
	extensions_folder = gen_file_structure({
		'empty' : {
			'__init__.py': '''
import egoros
from typing import Dict, Any

def init(config: Dict[str, Any]):
	config('config')
			'''
		},
		'full': {
			'__init__.py': '''
from egoros import *
from typing import Dict, Any

def init(config: Dict[str, Any]):
	config('config')	

def on_publish(topic_name: TopicName_t, msg):
	topic_name('topic_name')
	msg('msg')

def on_subscribe(node_name: NodeName_t, topic_name: TopicName_t):
	node_name('node_name')
	topic_name('topic_name')

def on_node_state_change(node_name: NodeName_t, state):
	node_name('node_name')
	state('state')

def on_orchestrator_state_change(state):
	state('orchestrator_state')
			'''
		}
	})
	
	extensions = load_extensions_from_path(extensions_folder.name)

	assert 'empty' in extensions
	assert 'full' in extensions

	# Mocks
	mock = Mock()

	empty = extensions['empty'].interface
	full = extensions['full'].interface

	assert full.on_publish is not None
	assert full.on_subscribe is not None
	assert full.on_node_state_change is not None
	assert full.on_orchestrator_state_change is not None

	empty.init_extension(mock)
	mock.assert_called_once_with('config')
	mock.reset_mock()

	full.init_extension(mock)
	mock.assert_called_once_with('config')
	mock.reset_mock()

	full.on_publish(mock, mock)
	mock.assert_has_calls([call('topic_name'), call('msg')], any_order=True)
	mock.reset_mock()

	full.on_subscribe(mock, mock)
	mock.assert_has_calls([call('node_name'), call('topic_name')], any_order=True)
	mock.reset_mock()

	full.on_node_state_change(mock, mock)
	mock.assert_has_calls([call('node_name'), call('state')], any_order=True)
	mock.reset_mock()

	full.on_orchestrator_state_change(mock)
	mock.assert_has_calls([call('orchestrator_state')], any_order=True)
	mock.reset_mock()
