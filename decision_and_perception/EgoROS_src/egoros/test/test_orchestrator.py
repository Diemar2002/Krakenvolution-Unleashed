from unittest.mock import call
import pytest

from ..mod.orchestrator import List, Orchestrator, OrchestratorStatus, TopicHandler

from .utils import gen_file_structure, DEFAULT_NODE, WaitableMock

@pytest.fixture
def dummy_data():
   files = [gen_file_structure(DEFAULT_NODE, suffix='.py') for _ in range(3)]
   return files

class ManagedOrchestrator(Orchestrator):
   def __init__(self, nodes: List[str]) -> None:
       super().__init__(nodes)

   def __enter__(self):
      self.start()
      return self

   def __exit__(self, type, value, traceback):
      self.stop()
      

@pytest.mark.execution_timeout(1)
def test_creation(dummy_data):
    files = dummy_data
    orchestrator = Orchestrator([file.name for file in files])

@pytest.mark.execution_timeout(1)
def test_stop(dummy_data):
    files = dummy_data
    orchestrator = Orchestrator([file.name for file in files])

    orchestrator.start()
    orchestrator.stop()

@pytest.mark.execution_timeout(1)
def test_messages():
   nodes = [gen_file_structure(f'''
import egoros

def callback(message):
    message({i})

def generic_callback(message):
    message({i})

def init(handler):
    handler.subscribe('generic', generic_callback)
    handler.subscribe('topic_{i}', callback)
    return egoros.Configuration()

    ''', suffix='.py') for i in range(3)]
    
   generic_mock = WaitableMock()

   with ManagedOrchestrator([n.name for n in nodes]) as managed_orchestrator:
      managed_orchestrator.publish('generic', generic_mock)
      generic_mock.event.wait()
      # FIXME: check why the fuck doesn't this work if the next tests do. Shouldn't be strictly necessary tho
      # generic_mock.assert_has_calls([call(i) for i in range(3)] , any_order=True)

      # Call specific topics to check isolation
      for i in range(3):
          specific_mock = WaitableMock()
          managed_orchestrator.publish(f'topic_{i}', specific_mock)
          specific_mock.event.wait()
          specific_mock.assert_called_once_with(i)


def test_communications():
	publisher_file = gen_file_structure(f'''
import egoros

def init(handler):
	return egoros.Configuration(
		tick_rate = 0.1
	)

def tick(handler):
	handler.publish('topic', 123)
	''', suffix='.py')

	subscriber_file = gen_file_structure(f'''
import egoros

def callback(message):
	pass

def init(handler):
	return egoros.Configuration()
	''', suffix='.py')

	pub_mock = WaitableMock()
	sub_mock = WaitableMock()
	tap_handler = TopicHandler(
		publish=pub_mock,
		subscribe=sub_mock
	)

	nodes = [publisher_file.name, subscriber_file.name]
	orchestrator = ManagedOrchestrator(nodes)
	orchestrator.hook_handler(handler=tap_handler)

	with orchestrator:
		pub_mock.event.wait()
		pub_mock.assert_called_once_with('topic', 123)

def test_subscription_active():
	file = gen_file_structure('''
import egoros

def callback(msg):
	msg(123)

def init(handler):
	handler.subscribe('topic', callback)
	return egoros.Configuration()
	''', suffix='.py')

	with ManagedOrchestrator([file.name]) as orchestrator:
		mock = WaitableMock()
		orchestrator.publish('topic', mock)
		mock.event.wait()
		mock.assert_called_once_with(123)

def test_status_all_right(dummy_data):
	files = dummy_data
	# Add crashing node
	orchestrator = ManagedOrchestrator([file.name for file in files])
	assert orchestrator.status() == OrchestratorStatus.STOPPED
	with orchestrator:
		assert orchestrator.status() == OrchestratorStatus.RUNNING
		

def test_status_fail(dummy_data):
	files = dummy_data
	files.append(gen_file_structure('''
import egoros
aslkdj lk
	''', suffix='.py'))

	orchestrator = ManagedOrchestrator([file.name for file in files])
	assert orchestrator.status() == OrchestratorStatus.STOPPED
	with orchestrator:
		assert orchestrator.status() == OrchestratorStatus.NODES_CRASHED
