import pytest
from .utils import gen_file_structure

from ..mod.dirtools import scan_folder_for_nodes
import os.path

DEFAULT_NODE = '''
import egoros
def init(handler):
    return handler.Configuration()
def tick(handler):
    pass
'''

def test_invalid_path():
    pathname = 'thisfolderdoesnotexist'

    with pytest.raises(FileNotFoundError):
        nodes = scan_folder_for_nodes(pathname)

def test_valid_path():
    folder = gen_file_structure({
        'node_1.py': DEFAULT_NODE,
        'node_2.py': DEFAULT_NODE,
        'package_node': {
            '__init__.py': DEFAULT_NODE,
            'another_file.py': '',
            'subfolder': {
                'test.py': DEFAULT_NODE
            }
        },
        'subfolder': {
            'node.py': DEFAULT_NODE,
            'dynamic.so': ''
        }
    })

    scan = scan_folder_for_nodes(folder.name)
    # Check if files are detected
    def check_present(path: str):
        to_check = os.path.join(folder.name, path)
        assert to_check in scan

    def check_not_present(path: str):
        to_check = os.path.join(folder.name, path)
        assert to_check not in scan

    check_present('node_1.py')
    check_present('node_2.py')
    check_present('package_node')
    check_not_present('package_node/subfolder')
    check_not_present('subfolder')
    check_present('subfolder/node.py')
    check_present('subfolder/dynamic.so')


