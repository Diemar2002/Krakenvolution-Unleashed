from __future__ import annotations

from ..mod.node import Node
from ..mod.orchestrator import Orchestrator
from ..mod.mod_loader import name_from_path
from .utils import WaitableMock, gen_file_structure

import yaml

def test_node_config():
	node_file = gen_file_structure('''
import egoros

def init(handler, config):
	assert config['integer'] == 123
	assert config['string'] == 'Kekos'
	assert config['boolean'] == True

	assert config['list'] == ['Kekos', 'Indie']
	assert config['map'] == {
		'key': 'value'
	}

	config['mock'](123)

	return egoros.Configuration()
	''', suffix='.py')

	config = f'''
nodes:
   {name_from_path(node_file.name)}:
      integer: 123
      string: "Kekos"
      boolean: true
      list:
         - Kekos
         - Indie
      map:
         key: "value"
'''

	orchestrator = Orchestrator(nodes=[node_file.name])
	
	config = yaml.unsafe_load(config)

	# Add mock to test if call has been run
	mock = WaitableMock()
	config['nodes'][name_from_path(node_file.name)]['mock'] = mock

	orchestrator.start(config=config['nodes'])
	mock.event.wait()
	mock.assert_called_once_with(123)
	orchestrator.stop()
