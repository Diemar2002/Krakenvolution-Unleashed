from __future__ import annotations
import pytest
from .utils import WaitableMock

# import ..mod.extensions.network as net
from ..extensions import network as net

def test_from_json_pub():
	DATA = '''
{
	"action": "publish",
	"topic": "example_topic",
	"data": 12345
}
	'''
	
	data = net.from_json(DATA)
	assert type(data) == net.PublishInstruction

	assert data.topic == 'example_topic'
	assert data.data == 12345

def test_from_json_sub():
	DATA = '''
{
	"action": "subscribe",
	"topic": "example_topic"
}
	'''

	data = net.from_json(DATA)
	assert type(data) == net.SubscribeInstruction

	assert data.topic == 'example_topic'

def test_from_json_errors():
	PUB_DATA_NODATA = '''
{
	"action": "publish",
	"topic": "example_topic",
}
	'''
	PUB_DATA_NOTOPIC = '''
{
	"action": "publish",
	"data": 12345
}
	'''

	NO_ACTION = '''
{
	"topic": "example_topic",
}
	'''

	SUB_DATA_NO_TOPIC = '''
{
	"action": "subscribe",
}
	'''

	with pytest.raises(ValueError):
		net.from_json(PUB_DATA_NODATA)
	with pytest.raises(ValueError):
		net.from_json(PUB_DATA_NOTOPIC)
	with pytest.raises(ValueError):
		net.from_json(NO_ACTION)
	with pytest.raises(ValueError):
		net.from_json(SUB_DATA_NO_TOPIC)



