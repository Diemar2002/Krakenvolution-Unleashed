import pytest

from .utils import gen_file_structure
from ..mod.mod_loader import load_node, name_from_path

import os.path as pth

def test_normal_node():
    file = gen_file_structure('''
import egoros
def init(handler):
    return egoros.Configuration(name='test')
def tick(handler):
    pass
                              ''', suffix='.py')
    
    node = load_node(file.name)
    # Check types
    assert hasattr(node.module, 'tick')
    assert hasattr(node.module, 'init')

def test_multiple_normal_node():
    file = gen_file_structure('''
import egoros
def init(handler):
    return egoros.Configuration(name='test')
def tick(handler):
    pass
    testvar = 0
                              ''', suffix='.py')

    node_1 = load_node(file.name)
    node_2 = load_node(file.name)

    # Check if the loaded modules are different
    node_2.module.testvar = 0
    node_1.module.testvar = 123

    assert node_2.module.testvar == 0
    node_2.module.testvar = 14
    assert node_1.module.testvar == 123 

	# Check important files
    assert len(node_1.important_files) == 1
    assert pth.samefile(node_1.important_files[0], file.name)



def test_normal_functions_loaded():
    file = gen_file_structure('''
import egoros
def init(handler):
    return egoros.Configuration(name='test')

def add(a, b):
    return a + b
                              ''', suffix='.py')

    node = load_node(file.name)
    assert node.module.add(1, 2) == 3

def test_normal_notfound():
    file = 'thisfiledoesnotexist.py'
    
    with pytest.raises(FileNotFoundError):
        load_node(file)

# TODO: check how this should behave

# def test_normal_node_no_init():
#     file = gen_file_structure('''
# import egoros
# def tick(handler):
#     pass
#                               ''', suffix='.py')
#
#     with pytest.raises(ModuleNotFoundError):
#         load_node(file.name)

def test_init_args():
    file_valid = gen_file_structure('''
import egoros
def init(handler):
    return egoros.Configuration(name='test')
                                    ''', suffix='.py')
    file_invalid_1 = gen_file_structure('''
import egoros
def init(handler, other):
    return egoros.Configuration(name='test')
                                    ''', suffix='.py')
    file_invalid_2 = gen_file_structure('''
import egoros
def init():
    return egoros.Configuration(name='test')
                                    ''', suffix='.py')

    node_valid = load_node(file_valid.name)

	# TODO: Think about this behaviour
    # with pytest.raises(ModuleNotFoundError):
    #     node_invalid_1 = load_node(file_invalid_1.name)
    # with pytest.raises(ModuleNotFoundError):
    #     node_invalid_1 = load_node(file_invalid_2.name)

def test_normal_no_tick():
    file = gen_file_structure('''
import egoros
def init(handler):
    return egoros.Configuration(name='test')
                              ''', suffix='.py')

    loaded = load_node(file.name)

    assert hasattr(loaded.module, 'init')

def test_folder_without_initpy():
    folder = gen_file_structure({
        'test.py': 'This is a test file, so it should not be imported'
    })

    with pytest.raises(ModuleNotFoundError):
        load_node(folder.name)

def test_package_node():
    folder = gen_file_structure({
        '__init__.py': '''
import egoros
def init(handler):
    return egoros.Configuration(name='test')
                       '''
    })

    node = load_node(folder.name)

def test_package_node_indirect():
    folder = gen_file_structure({
        '__init__.py': '''
from .file import init
from .file import add
                       ''',
        'file.py': '''
import egoros
def init(handler):
    return egoros.Configuration(name='test')

def add(a, b):
    return a + b
                   '''
    })

    node = load_node(folder.name)

    assert node.module.add(1, 2) == 3

def test_dynamic_node():
    pytest.skip('Test not implemented')


def test_name_from_path():
	assert name_from_path('/home/test/node.py') == 'node'
	assert name_from_path('/home/test/node.so') == 'node'
	assert name_from_path('/home/test/node') == 'node'


def test_package_important_files():
	folder = gen_file_structure({
		'__init__.py': '''
from .file import init
from .file import add
					   ''',
		'file.py': '''
import egoros
def init(handler):
	return egoros.Configuration(name='test')

def add(a, b):
	return a + b
				   '''
	})

	node = load_node(folder.name)

	assert len(node.important_files) == 2
	assert any(pth.samefile(file, pth.join(folder.name, '__init__.py')) for file in node.important_files)
	assert any(pth.samefile(file, pth.join(folder.name, 'file.py')) for file in node.important_files)
