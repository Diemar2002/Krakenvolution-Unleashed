from typing import Any, Dict, Union
import tempfile
import os
import os.path

import logging

log = logging.getLogger(__name__)

from unittest.mock import Mock
import threading

# Some useful variables
DEFAULT_NODE = '''
import egoros

def init(handler, config):
    return egoros.Configuration()

def tick(handler):
    pass
'''

DEFAULT_TICKLESS_NODE = '''
import egoros

def init(handler, config):
    return egoros.Configuration()
'''


def gen_file_structure(structure: Union[str, Dict[str, Any]], suffix: str = '') -> Union[tempfile.TemporaryDirectory, tempfile.TemporaryFile]:
    def recursion(path: str, structure: Dict[str, Any]):
        for name, value in structure.items():
            # Check if file or directory
            if type(value) == dict:
                # Create folder
                new_path = os.path.join(path, name)
                os.mkdir(new_path)
                recursion(new_path, value)
            elif type(value) == str:
                # Create file and write contents
                with open(os.path.join(path, name), 'w+') as file:
                    file.write(value)
        pass
    # Check type
    if type(structure) == str:
        tmpfile = tempfile.NamedTemporaryFile(suffix=suffix)
        with open(tmpfile.name, 'w+') as file:
            file.write(structure)
        return tmpfile
    else:
        # Generate temp folder
        tmpdir = tempfile.TemporaryDirectory()
        recursion(tmpdir.name, structure)
        return tmpdir

def test_gen_folders():
    folders = gen_file_structure({
        'file.txt': 'This is a test',
        'test.py': 'This is another test',
        'folder': {
            'test.lua': 'This is the final test'
        }
    })
    log.debug(f'Folder name: {folders.name}')
    
    def join(*args):
        return os.path.join(folders.name, *args)
    # Check if files where created
    assert os.path.isfile(join('file.txt'))
    assert os.path.isfile(join('test.py'))
    assert os.path.isdir(join('folder'))
    assert os.path.isfile(join('folder', 'test.lua'))

    def test_content(path: str, expected: str):
        with open(path, 'r') as file:
            content = file.read()
            assert content == expected

    test_content(join('file.txt'), 'This is a test')
    test_content(join('test.py'), 'This is another test')
    test_content(join('folder', 'test.lua'), 'This is the final test')


class WaitableMock(Mock):
    def __init__(self, spec: Any | None = None, side_effect: Any | None = None, return_value: Any = ..., wraps: Any | None = None, name: Any | None = None, spec_set: Any | None = None, parent: Any | None = None, _spec_state: Any | None = None, _new_name: Any = "", _new_parent: Any | None = None, **kwargs: Any) -> None:
        super().__init__(spec, side_effect, return_value, wraps, name, spec_set, parent, _spec_state, _new_name, _new_parent, **kwargs)
        self.event = threading.Event()

    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        self.event.set()
        return super().__call__(*args, **kwargs)


def test_gen_file():
    file = gen_file_structure('TEST')
    log.debug(f'Temp file: {file.name}')

    # Check if file exists
    assert os.path.isfile(file.name)
    with open(file.name, 'r') as file:
        content = file.read()
        assert content == 'TEST'

def test_suffix():
    file = gen_file_structure('TEST', '.py')
    assert file.name.endswith('.py')

def test_waitable_mock():
    mock = WaitableMock()
    def worker():
        mock(123)

    thread = threading.Thread(target=worker)
    thread.start()

    mock.event.wait(0.2)
    mock.assert_called_with(123)

