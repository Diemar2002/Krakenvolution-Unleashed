from datetime import timedelta
from typing import List
import pytest

from .test_dirutils import DEFAULT_NODE

from ..mod.orchestrator import Orchestrator

from .utils import WaitableMock, gen_file_structure
from ..mod.node import Node
from ..mod.node_handler import NodeState
import os.path as path

@pytest.mark.execution_timeout(1)
def test_normal_behaviour():
	CONTENT = '''
import egoros

def callback(message):
	message({value})

def init(handler: egoros.TopicHandler, config):
	config()
	handler.subscribe('topic', callback)
	return egoros.Configuration()
	'''
	file = gen_file_structure(CONTENT.format(value=123), suffix='.py')

	reload_mock = WaitableMock()
	config = { path.splitext(path.basename(file.name))[0]: reload_mock }

	orchestrator = Orchestrator(nodes=[file.name])

	first_mock = WaitableMock()
	second_mock = WaitableMock()

	orchestrator.enable_hot_reloading()
	orchestrator.start(config=config)

	reload_mock.event.wait()
	reload_mock.event.clear()
	orchestrator.publish('topic', first_mock)
	first_mock.event.wait()
	first_mock.assert_called_once_with(123)

	with open(file.name, 'w') as f:
		f.write(CONTENT.format(value=345))

	reload_mock.event.wait()
	orchestrator.publish('topic', second_mock)
	second_mock.event.wait()
	second_mock.assert_called_once_with(345)
	assert first_mock.call_count == 1

	orchestrator.stop()
