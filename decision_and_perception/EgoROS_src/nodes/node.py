import egoros

config = {}

def init(handler: egoros.TopicHandler, _config):
	global config
	config = _config
	return egoros.Configuration()

def tick(handler: egoros.TopicHandler):
	handler.publish('topic', f'Ahora debería funcionar {config}')
	pass
