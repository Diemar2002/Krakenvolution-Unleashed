from __future__ import annotations
from dataclasses import dataclass, asdict
from sys import argv
from typing import List
from datamodel import LidarRotation, datapoints_to_json
import datetime as dt
import json

@dataclass
class DataPoint:
	timestamp: dt.datetime
	rotation: LidarRotation

if len(argv) < 2:
	print("Error: No file specified")
	exit()

import pickle
with open(argv[1], 'rb') as file:
	data: List[DataPoint] = pickle.load(file)

	print(json.dumps(datapoints_to_json(data)))
