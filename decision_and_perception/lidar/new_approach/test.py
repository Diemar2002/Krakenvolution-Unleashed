from dataclasses import dataclass
from io import TextIOWrapper
import pygame
from typing import Iterable, List, Tuple
from clustering import estimate_objects, DBSCANParams
from filter import filter_objs
from listen_to_lidar import lidar_measure_generator
import numpy as np

import pickle
import datetime as dt

from datamodel import LidarRotation, cartesian_to_polar_matrix, new_polar_matrix, polar_to_cartesian_matrix

lidar_rot_gen = lidar_measure_generator('/dev/ttyUSB0')

OUTPUT_FILE: str | None = 'reverse.pkl'
# OUTPUT_FILE: str | None = None
# Initialize pygame
pygame.init()

# Screen dimensions
WIDTH, HEIGHT = 800, 600
FONT = pygame.font.SysFont('monospace', 15)

def draw_text(text, coords, surface, color=(255, 255, 255)):
	global FONT
	text_surface = FONT.render(text, True, color)
	surface.blit(text_surface, coords)

SCALE = min(WIDTH, HEIGHT) / 12.0

# Colors
BACKGROUND_COLOR = (18, 18, 18)
CLICK_COLOR = (45, 45, 45)
CENTER_COLOR = (145,195,43)

COLORS = [
	(255, 181, 73),
	(241, 112, 118),
	(46, 162, 219),
	(204, 35, 201),
	(146, 231, 203)
]
import random

camera_offset = np.array([WIDTH / 2, HEIGHT / 2])

def draw_circle_with_offset(screen, color, center, radius, width = 0):
    center_with_offset = ((center[0] + camera_offset[0]), (center[1] + camera_offset[1]))
    pygame.draw.circle(screen, color, center_with_offset, radius, width)

def random_color() -> Tuple[int, int, int]:
	return random.sample(
		population=COLORS,
		k=1
	)[0]

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Click to add points")


params = DBSCANParams(
	eps=20,
	min_samples=2
)


def ndarray_to_tuples(arr: np.ndarray) -> Iterable[Tuple[int, int]]:
    """Converts a 2D numpy array to an iterable of tuples."""
    return (tuple(map(int, point)) for point in arr)

def display_points(points_list: List[Tuple[int, int]], color: Tuple[int, int, int]) -> None:
	"""Display a list of points on the screen with a specified color."""
	for point in points_list:
		draw_circle_with_offset(screen, color, point, 5)

# Open file
@dataclass
class DataPoint:
	timestamp: dt.datetime
	rotation: LidarRotation

stored_data: List[DataPoint] = []

try:
	running = True
	while running:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False
			if event.type == pygame.MOUSEBUTTONDOWN:
				# Append the x, y of the mouse click to the points list
				# points = np.vstack([points, np.array(event.pos) - camera_offset])
				pass
			if event.type == pygame.KEYDOWN:
				# if event.key == pygame.K_r:
				# 	points = np.zeros((0, 2), dtype=float)
				pass
		
		# Fill screen with white
		pygame.transform.threshold
		screen.fill(BACKGROUND_COLOR)
		draw_circle_with_offset(
			screen,
			(255, 255, 255),
			(0, 0),
			20
		)
		
		# Display the points
		# rotation = LidarRotation(
		# 	speed=0,
		# 	confidence=None,
		# 	coords=new_polar_matrix()
		# )
		rotation = lidar_rot_gen.__next__()
		display_points(polar_to_cartesian_matrix(rotation.coords), CLICK_COLOR)

		if OUTPUT_FILE is not None:
			datapoint = DataPoint(
				timestamp=dt.datetime.now(),
				rotation=rotation
			)
			stored_data.append(datapoint)

		objects = filter_objs(estimate_objects(rotation, params))
		for object in  objects:
			display_points(np.array([object.position]), CENTER_COLOR)
			# Draw circle
			draw_circle_with_offset(
				screen,
				CENTER_COLOR,
				object.position,
				object.size,
				3
			)

			draw_text(
				f'Size: {object.size:1.4}',
				(object.position + np.array([10.0, 10.0])) + camera_offset,
				screen
			)

			display_points(object.points, random_color())
		
		pygame.display.flip()
except KeyboardInterrupt:
	print(f'Stopping program with {len(stored_data)} samples')

if OUTPUT_FILE is not None:
	with open(OUTPUT_FILE, 'wb+') as file:
		print(f'Storing {len(stored_data)} samples')
		pickle.dump(stored_data, file)

pygame.quit()
