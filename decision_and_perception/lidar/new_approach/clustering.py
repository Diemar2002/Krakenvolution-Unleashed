from __future__ import annotations
from dataclasses import asdict, dataclass, field
from itertools import filterfalse, groupby, starmap
from typing import Iterator, List, Any
from datamodel import POLAR_PHI_INDEX, CartessianCoords, CartessianCoordsMatrix, LidarRotation, cartesian_to_polar, cartesian_to_polar_matrix, new_cartessian_coords, new_cartessian_matrix, polar_to_cartesian_matrix
from sklearn.cluster import DBSCAN
import numpy as np

@dataclass
class Object:
	position: CartessianCoords = field(default_factory=new_cartessian_coords)
	size: float = 0.0
	points: CartessianCoordsMatrix = field(default_factory=new_cartessian_matrix)

@dataclass
class DBSCANParams:
	eps: float
	min_samples: int

def estimate_objects(scan: LidarRotation, params: DBSCANParams) -> List[Object]:
	db = DBSCAN(**asdict(params))

	cartessian_coords = polar_to_cartesian_matrix(scan.coords)

	labels = db.fit_predict(cartessian_coords)

	groups: Iterator[CartessianCoordsMatrix] =  starmap( # Generator CartessianCoordMatrix
		lambda _, x: CartessianCoordsMatrix(np.array(list(starmap( # -> CartessianCoordsMatrix 
			lambda label, coord: coord, # Generator label coord -> Generator coord
			x # x :: (int, Generator (label, coord))

		)))),
		filterfalse(
			lambda x: x[0] == -1, # x :: (int, Generator (label, coord))
			groupby(
				zip(labels, cartessian_coords),
				key = lambda x: x[0]
			)
		)
	)

	objects: List[Object] = []
	for group in groups:
		# Now it's maths time!
		polar_repr = cartesian_to_polar_matrix(group)
		# Get min and max by angle (god fucking why)
		median_angle = np.median(polar_repr, axis=0)[POLAR_PHI_INDEX]
		rotation_matrix = np.array([[np.cos(median_angle), -np.sin(median_angle)],
                                [np.sin(median_angle), np.cos(median_angle)]])

		rotated_cartessian = np.dot(group, rotation_matrix.T)
		angles = np.arctan2(rotated_cartessian[:, 1], rotated_cartessian[:, 0])
		# Get extreme points
		zipped_cartessian_for_min = enumerate(angles)
		zipped_cartessian_for_max = enumerate(angles)
		max_point_index = max(zipped_cartessian_for_min, key=lambda elem: elem[1])[0]
		min_point_index = min(zipped_cartessian_for_max, key=lambda elem: elem[1])[0]

		extremes_v = group[max_point_index] - group[min_point_index]
		obj_size = float(np.linalg.norm(extremes_v) / 2)
		middle_point = (extremes_v / 2) + group[min_point_index]
		center = middle_point + (middle_point / np.linalg.norm(middle_point)) * obj_size

		new_object = Object (
			points = group,
			position = center,
			size = obj_size
		)

		objects.append(new_object)

	return objects
		

		
