from __future__ import annotations
from typing import List, NewType
import numpy as np

PolarCoords = NewType('PolarCoords', np.ndarray) # Of shape [r, phi]
CartessianCoords = NewType('CartessianCoords', np.ndarray) # Of shape [x, y]
PolarCoordsMatrix = NewType('PolarCoordsMatrix', np.ndarray) # Of shape [N, 2], N being any number of points
CartessianCoordsMatrix = NewType('CartessianCoordsMatrix', np.ndarray) # Of shape [N, 2]

POLAR_PHI_INDEX = 1
POLAR_RADIUS_INDEX = 0

# Creation methods
def new_polar_coords() -> PolarCoords:
	return PolarCoords(np.zeros(2))

def new_cartessian_coords() -> CartessianCoords:
	return CartessianCoords(np.zeros(2))

def new_polar_matrix() -> PolarCoordsMatrix:
	return PolarCoordsMatrix(np.zeros((0, 2)))

def new_cartessian_matrix() -> CartessianCoordsMatrix:
	return CartessianCoordsMatrix(np.zeros((0, 2)))

def polar_to_cartesian(coords: PolarCoords) -> CartessianCoords:
    """
    Convert polar coordinates to cartesian coordinates.
    
    >>> polar_to_cartesian(np.array([1, np.pi/4]))
    array([0.70710678, 0.70710678])
    
    >>> polar_to_cartesian(np.array([2, np.pi/2]))
    array([1.2246468e-16, 2.0000000e+00])
    """
    r, phi = coords
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    return CartessianCoords(np.array([x, y]))

def cartesian_to_polar(coords: CartessianCoords) -> PolarCoords:
    """
    Convert cartesian coordinates to polar coordinates.
    
    >>> cartesian_to_polar(np.array([0.70710678, 0.70710678]))
    array([1.        , 0.78539816])
    
    >>> cartesian_to_polar(np.array([1.2246468e-16, 2.0000000e+00]))
    array([2.        , 1.57079633])
    """
    x, y = coords
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return PolarCoords(np.array([r, phi]))

def polar_to_cartesian_matrix(coords: PolarCoordsMatrix) -> CartessianCoordsMatrix:
    """
    Convert a matrix of polar coordinates to cartesian coordinates.
    
    >>> polar_to_cartesian_matrix(np.array([[1, np.pi/4], [2, np.pi/2]]))
    array([[7.07106781e-01, 7.07106781e-01],
           [1.22464680e-16, 2.00000000e+00]])
    """
    r, phi = coords[:, 0], coords[:, 1]
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    return CartessianCoordsMatrix(np.column_stack((x, y)))

def cartesian_to_polar_matrix(coords: CartessianCoordsMatrix) -> PolarCoordsMatrix:
    """
    Convert a matrix of cartesian coordinates to polar coordinates.
    
    >>> cartesian_to_polar_matrix(np.array([[0.70710678, 0.70710678], [1.2246468e-16, 2.0000000e+00]]))
    array([[1.        , 0.78539816],
           [2.        , 1.57079633]])
    """
    x, y = coords[:, 0], coords[:, 1]
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return PolarCoordsMatrix(np.column_stack((r, phi)))

# LIDAR data
from dataclasses import dataclass, field

@dataclass
class LidarData:
	start_angle: float = 0.0
	end_angle: float = 0.0
	crc_check: int = 0
	speed: int = 0
	time_stamp: int = 0
	confidence: np.ndarray = field(default_factory=lambda: np.ndarray([], dtype=float))
	coords: PolarCoordsMatrix = field(default_factory=new_polar_matrix)

@dataclass
class LidarRotation:
	speed: int # Averaging of all speeds
	confidence: np.ndarray
	coords: PolarCoordsMatrix


# For storing samples
import datetime as dt
@dataclass
class DataPoint:
	timestamp: dt.datetime
	rotation: LidarRotation

def json_to_datapoints(json_data) -> List[DataPoint]:
    data_points = []

    for datum in json_data:
        timestamp = dt.datetime.fromisoformat(datum['timestamp'])
        rotation_dict = datum['rotation']
        rotation = LidarRotation(
            speed=rotation_dict['speed'],
            confidence=np.array(rotation_dict['confidence']),
            coords=np.array(rotation_dict['coords'])
        )
        data_points.append(DataPoint(timestamp=timestamp, rotation=rotation))

    return data_points

def datapoints_to_json(datapoints: List[DataPoint]):
	return [{
		'timestamp': datum.timestamp.isoformat(),
		'rotation': {
			'speed': datum.rotation.speed,
			'confidence': datum.rotation.confidence.tolist(),
			'coords': datum.rotation.coords.tolist()
		}
	} for datum in datapoints]
