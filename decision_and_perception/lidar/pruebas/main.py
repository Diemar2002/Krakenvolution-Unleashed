import pygame
import math
from listen_to_lidar import listen_to_lidar_full_rotation  # Replace with your module name
from clustering import estimate_objects

pygame.init()
screen = pygame.display.set_mode((800, 800))
pygame.display.set_caption('Lidar Visualization')
clock = pygame.time.Clock()

PORT = '/dev/ttyUSB0'
lidar_data_gen = listen_to_lidar_full_rotation(PORT)


SCALE = 1
def draw_lidar_points(distances, angles):
	for angle, distance in zip(angles, distances):
		x = int(400 + distance * SCALE * math.cos(math.radians(angle)))
		y = int(400 + distance * SCALE * math.sin(math.radians(angle)))
		pygame.draw.circle(screen, (255, 255, 255), (x, y), 2)

running = True
while running:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False

	screen.fill((0, 0, 0))  # Attempt to clear the screen here

	try:
		data = next(lidar_data_gen)
		distances = data.distance_i
		angles = data.angle_i
		print(distances.__len__())
		print(f"New data received: {len(distances)} points")  # Debugging print
		draw_lidar_points(distances, angles)
		pygame.display.update()
	except StopIteration:
		pass

