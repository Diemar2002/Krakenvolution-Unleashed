from __future__ import annotations
from clustering import DBSCANParams, estimate_objects
from listen_to_lidar import listen_to_lidar_full_rotation

import pygame
import pygame_gui
import numpy as np
import random

# Initialize pygame
pygame.init()

# Set up display
width, height = 800, 600
window = pygame.display.set_mode((width, height))
pygame.display.set_caption('Coordinate Recorder')

BACKGROUND_COLOR = (18, 18, 18)
CLICK_COLOR = (45, 45, 45)
CENTER_COLOR = (145,195,43)
CLICK_SIZE = 6
BORDER = 3

# UI
manager = pygame_gui.UIManager((width, height))

eps_slider = pygame_gui.elements.UIHorizontalSlider(
    relative_rect=pygame.Rect((10, 10), (200, 20)),
    start_value=0.1,
    value_range=(0.01, 1.0),
    manager=manager
)
min_samples_slider = pygame_gui.elements.UIHorizontalSlider(
    relative_rect=pygame.Rect((10, 40), (200, 20)),
    start_value=4,
    value_range=(1, 10),
    manager=manager
)


def offset() -> np.ndarray:
	width = window.get_width()
	height = window.get_height()
	return np.array([width / 2.0, height / 2.0])

# Define the array to hold coordinates
def mouse_generator():
	coords_array = np.empty((0, 2), int)
	while True:
		event = pygame.event.wait()
		if event.type == pygame.MOUSEBUTTONDOWN:
			x, y = event.pos
			coords_array = np.vstack([coords_array, 
								(np.array([x, y]) - offset()) * SCALE
							])
			yield coords_array
		elif event.type == pygame.QUIT:
			raise StopIteration

DBSCAN_PARAMS = DBSCANParams(
	eps=0.1,
	min_samples=4
)
LIDAR_MAX_RANGE = 3.0
SCALE = LIDAR_MAX_RANGE / min(window.get_width(), window.get_height())

# REAL LIDAR
lidar_gen = listen_to_lidar_full_rotation('/dev/ttyUSB0')
lidar_gen = map(
    lambda elem: np.column_stack((
        elem.distance_i * np.cos(np.radians(elem.angle_i)),
        elem.distance_i * np.sin(np.radians(elem.angle_i))
    )) / 100,
    lidar_gen
)

# Run the game loop
running = True
# for coords_array in mouse_generator():
for coords_array in lidar_gen:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			break
		manager.process_events(event)

	manager.update(1 / 100)
	

	# Get estimated objects
	DBSCAN_PARAMS = DBSCANParams(
        eps=eps_slider.get_current_value(),
        min_samples=int(min_samples_slider.get_current_value())
    )
	objects = estimate_objects(coords_array, DBSCAN_PARAMS)

	# Draw
	window.fill(BACKGROUND_COLOR)

	pygame.draw.circle(
			window,
			(255, 255, 255),
			tuple(offset()),
			20
			)
		# All points
	for point in coords_array:
		center = tuple((point / SCALE + offset()).astype(int))
		pygame.draw.circle(
					window,
					CLICK_COLOR,
					center,
					CLICK_SIZE
				)

		# Calculated objects
	colored_objects = zip(
		((random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)) for _ in objects),
		objects
	)
	for color, object in colored_objects:
		center = tuple((object.position / SCALE + offset()).astype(int))
		pygame.draw.circle(
					window,
					color,
					center,
					(object.size / SCALE) * 0.5,
					BORDER
				)

		# Draw all composing points
		for point in object.points:
			center = tuple((point / SCALE + offset()).astype(int))
			pygame.draw.circle(
						window,
						color,
						center,
						CLICK_SIZE * 0.5
					)


	manager.draw_ui(window)

	pygame.display.flip()

# Quit pygame
pygame.quit()
