from __future__ import annotations
from dataclasses import dataclass, field
from itertools import groupby, starmap, filterfalse
from typing import Iterable, List

from sklearn.cluster import DBSCAN
import numpy as np

@dataclass
class Object:
	position: np.ndarray = field(default_factory=lambda: np.ndarray([0, 0]))
	size: float = 0
	points: np.ndarray = field(default_factory=lambda: np.ndarray([]))

@dataclass
class DBSCANParams:
	eps: float
	min_samples: int

def estimate_objects(scan: np.ndarray, params: DBSCANParams) -> List[Object]:
	db = DBSCAN(
		eps=params.eps, 
		min_samples=params.min_samples
	)

	labels = db.fit_predict(scan)
	unique_labels = len(set(labels) - set((1,)))

	groups: Iterable = starmap(
				lambda _, g: g,
				filterfalse (
					lambda elem: elem[0] == -1,
					groupby(zip(labels, scan), key=lambda elem: elem[0])
				)
			)

	objects: List[Object] = []
	for g in groups:
		object = Object()
		filtered_generator = map(lambda e: e[1], g)
		points = np.array(list(filtered_generator), dtype=np.float32)
	
		radii = np.sqrt(np.sum(points**2, axis=1))
		angles = np.degrees(np.arctan2(points[:, 1], points[:, 0]))
		polar_coords = np.column_stack((radii, angles))  # Stack them into a 2D array

		# Calculate size
		min_by_angle = min(polar_coords, key=lambda elem: elem[0])
		max_by_angle = max(polar_coords, key=lambda elem: elem[0])

		min_x = min_by_angle[0] * np.cos(np.radians(min_by_angle[1]))
		min_y = min_by_angle[0] * np.sin(np.radians(min_by_angle[1]))
		min_cartesian = np.array([min_x, min_y])

		# Convert polar to Cartesian for max_by_angle
		max_x = max_by_angle[0] * np.cos(np.radians(max_by_angle[1]))
		max_y = max_by_angle[0] * np.sin(np.radians(max_by_angle[1]))
		max_cartesian = np.array([max_x, max_y])

		min_to_max = max_cartesian - min_cartesian
		between_min_max = (max_cartesian + min_cartesian) / 2.0
		between_min_max_norm = between_min_max / np.linalg.norm(between_min_max)
		
		object.position = between_min_max + between_min_max_norm * (np.linalg.norm(min_to_max) / 2.0)
		object.size = np.linalg.norm(min_to_max)


		object.points = points

		objects.append(object)


	return objects


