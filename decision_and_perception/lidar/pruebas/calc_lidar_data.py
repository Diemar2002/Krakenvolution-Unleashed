import numpy as np
from dataclasses import dataclass, field

@dataclass
class LidarData:
    start_angle: float = 0.0
    end_angle: float = 0.0
    crc_check: int = 0
    speed: int = 0
    time_stamp: int = 0
    confidence_i: np.ndarray = field(default_factory=lambda: np.array([], dtype=int))
    angle_i: np.ndarray = field(default_factory=lambda: np.array([], dtype=float))
    distance_i: np.ndarray = field(default_factory=lambda: np.array([], dtype=float))


def calc_lidar_data(packet):
    def get_byte(n: int) -> str:
        if n == -1:
            return packet[n * 2:]
        return packet[n * 2:(n + 1) * 2]

    speed = int(get_byte(1) + get_byte(0), 16)
    start_angle = float(int(get_byte(3) + get_byte(2), 16)) / 100
    end_angle = float(int(get_byte(-4) + get_byte(-5), 16)) / 100
    time_stamp = int(get_byte(-2) + get_byte(-3), 16)
    crc_check = int(get_byte(-1), 16)
    
    num_measurements = 12

    distance_i = np.zeros(num_measurements)
    confidence_i = np.zeros(num_measurements)
    angle_i = np.zeros(num_measurements)

    if end_angle > start_angle:
        angle_step = float(end_angle - start_angle) / num_measurements
    else:
        angle_step = float((end_angle + 360) - start_angle) / num_measurements

    def circle(deg):
        return deg - 360 if deg >= 360 else deg

    for i in range(num_measurements):
        measurement_position = 3 * i
        distance_bytes = get_byte(5 + measurement_position) + get_byte(4 + measurement_position)
        
        distance_i[i] = int(distance_bytes, 16) / 10  # centimeters
        confidence_i[i] = int(get_byte(6 + measurement_position), 16)
        angle_i[i] = circle(start_angle + (angle_step * i))

    return LidarData(start_angle, end_angle, crc_check, speed, time_stamp, confidence_i, angle_i, distance_i)
