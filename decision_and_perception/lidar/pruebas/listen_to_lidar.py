from typing import Generator
import serial
from calc_lidar_data import LidarData, calc_lidar_data
import numpy as np
from copy import deepcopy

import serial

def listen_to_lidar(port: str = '/dev/tty.usbserial-0001'):
    serial_port = serial.Serial(port=port, baudrate=230400, timeout=5.0, bytesize=8, parity='N', stopbits=1)
    packet_length = 4 + (3 * 12) + 4 + 1  # in bytes
    last_byte_was_header = False
    buffer = ""

    while True:
        byte = serial_port.read()
        byte_as_int = int.from_bytes(byte, 'big')

        if byte_as_int == 0x54:
            buffer += byte.hex()
            last_byte_was_header = True
            continue

        if last_byte_was_header and byte_as_int == 0x2c:
            buffer += byte.hex()

            if len(buffer[0:-4]) == packet_length * 2:
                lidar_data = calc_lidar_data(buffer[0:-4])
                yield lidar_data  # Yielding the new lidar_data object

            buffer = ""
        else:
            buffer += byte.hex()

        last_byte_was_header = False



PACKETS_PER_REVOLUTION = 34

def listen_to_lidar_full_rotation(port: str):
	lidar_gen = listen_to_lidar(port)

	data = LidarData()
	for num, packet in enumerate(lidar_gen):
		data.angle_i = np.concatenate((data.angle_i, packet.angle_i))
		data.distance_i = np.concatenate((data.distance_i, packet.distance_i))
		data.confidence_i = np.concatenate((data.confidence_i, packet.confidence_i))

		if num % PACKETS_PER_REVOLUTION == 0:
			data_to_send = deepcopy(data)
			data = LidarData()
			yield data_to_send

def stop(serial_port):
    serial_port.close()
