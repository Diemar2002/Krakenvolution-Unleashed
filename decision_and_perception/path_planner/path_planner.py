import networkx as nx 
import matplotlib.pyplot as plt

def pose2node(G, X, Y):
  # Encontrar el nodo más cercano al punto dado
  min_distance = float('inf')
  nearest_node = None

  for node, data in G.nodes(data=True):
      if 'pos' in data:
        node_position = data['pos']
      distance = ((X - node_position[0])**2 + (Y - node_position[1])**2)**0.5
      if distance < min_distance:
          min_distance = distance
          nearest_node = node
  
  return nearest_node

def generate_pitch(d_nodos):

  # Grafo vacio
  G = nx.Graph()

  # Dimensiones del campo
  ancho = 2
  largo = 3

  # Número de nodos 
  n_nodos_ancho = int(ancho/d_nodos)
  n_nodos_largo = int(largo/d_nodos)

  # Agregar nodos al grafo con posiciones físicas del campo
  for i in range(n_nodos_ancho):
      for j in range(n_nodos_largo):
        node_id = (i, j)
        node_position = (i*d_nodos, j*d_nodos)
        G.add_node(node_id, pos=node_position)
      
  # Agregamos aristas al grafo.
  for i in range(n_nodos_ancho):
    for j in range(n_nodos_largo):
      if i < n_nodos_ancho - 1:
        G.add_edge((i,j), (i+1, j))
      if j < n_nodos_largo - 1:
        G.add_edge((i,j), (i,j+1))

  return G

def add_obstacle(G, left, right, bottom, top):

  # Eliminar los nodos del área del obstáculo del grafo
  G.remove_nodes_from([(i, j) for i in range(left, right + 1) for j in range(bottom, top + 1)])

  return G

def draw_obstacle(d_nodos, left, right, bottom, top):
  # Dibujar el rectángulo del obstáculo
  obstacle_rectangle = plt.Rectangle((left*d_nodos, bottom*d_nodos),
                                     (right - left)*d_nodos,
                                     (top - bottom)*d_nodos,
                                     edgecolor='black', facecolor='white')
  plt.gca().add_patch(obstacle_rectangle)

def get_path(G, init, obj):
  
  # Coordenadas
  Xi, Yi = init
  Xf, Yf = obj

  # Transformación al grafo
  init_node = pose2node(G, Xi, Yi)
  obj_node = pose2node(G, Xf, Yf)

  # Aplicar algoritmo solucionador
  path = nx.astar_path(G, source=init_node, target=obj_node, heuristic=None)

  # path = nx.bellman_ford_path(G, source=init_node, target=obj_node)
  # path = nx.dijkstra_path(G, source=init_node, target=obj_node)

  return path

def draw_pitch_graph(G, positions, sizes, node_colors, edge_colors):
  # Dibujar el grafo
  plt.figure(figsize=(2*5, 3*5))
  nx.draw(G, pos=positions, with_labels=False, node_size=sizes, node_color=node_colors, font_size=1, edge_color=edge_colors, width=2)
  plt.title('Grafo del terreno de juego')
  plt.xlabel('Distancia (metros)')
  plt.ylabel('Distancia (metros)')
  plt.grid(visible=True)


if __name__ == '__main__':

  # Inicio y objetivo 
  Xi = 10/100
  Yi = 30/100

  Xf = 180/100
  Yf = 30/100

  # Definir las coordenadas del obstáculo rectangular (modificar según sea necesario)
  left    = 5
  right   = 10
  top     = 8
  bottom  = 0

  # Distancia entre nodos
  d_nodos = 10/100

  # Generamos el campo
  G = generate_pitch(d_nodos)

  # Añadimos un obtáculo/s
  G = add_obstacle(G, left, right, bottom, top)
  G = add_obstacle(G, left+5, right+2, top+1, top+10)
  G = add_obstacle(G, left, right+2, top+10, top+12)
  G = add_obstacle(G, left+5, 16, bottom+6, top+3)

  # Con posición inicial y final resolvemos el path
  path = get_path(G, [Xi, Yi], [Xf, Yf])

  # VISUALIZACIÓN #
  init_node = pose2node(G, Xi, Yi)
  obj_node = pose2node(G, Xf, Yf)

  # Crear una lista de tuplas que representen las aristas en el camino
  path_edges = [(path[i], path[i+1]) for i in range(len(path) - 1)]
  # Marcar el camino en el grafo con un color diferente
  path_colors = ['purple' if edge in path_edges or (edge[1], edge[0]) in path_edges else 'gray' for edge in G.edges()]

  # Marcar el nodo más cercano con un color diferente en el grafo
  node_colors = ['green' if node == init_node else 'red' if node == obj_node else 'purple' if node in path else 'skyblue' for node in G.nodes()]
  # Tamaño de los nodos
  node_sizes = [70 if node in [obj_node, init_node] else 30 if node in path else 10 for node in G.nodes()]
  # Obtener posiciones
  node_positions = {node: data['pos'] for node, data in G.nodes(data=True)}

  draw_pitch_graph(G, node_positions, node_sizes, node_colors, path_colors)
  draw_obstacle(d_nodos, left, right, bottom, top)
  draw_obstacle(d_nodos, left+5, right+2, top+1, top+10)
  draw_obstacle(d_nodos, left, right+2, top+10, top+12)
  draw_obstacle(d_nodos, left+5, 16, bottom+6, top+3)

  plt.show()
